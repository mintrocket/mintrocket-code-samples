import UIKit
import DITranquillity
import RxSwift

final class MainPart: DIPart {
    static func load(container: DIContainer) {
        container.register(MainPresenter.init)
            .as(MainEventHandler.self)
            .lifetime(.objectGraph)
    }
}

// MARK: - Presenter

final class MainPresenter {
    private weak var view: MainViewBehavior!
    private var router: MainRoutable!

    private let bag: DisposeBag = DisposeBag()
    private var items: [Account] = []
    private var currentIndex: Int = 0

    private let sessionService: SessionService
    private let accountService: AccountService

    init(sessionService: SessionService,
         accountService: AccountService) {
        self.sessionService = sessionService
        self.accountService = accountService
    }

    private lazy var defaultErrorHandling: Action<Error> = { [weak self] error in
        self?.router.show(error: error)
    }

    private var currentAccount: Account? {
        if self.currentIndex >= self.items.count {
            return nil
        }
        return self.items[self.currentIndex]
    }
}

extension MainPresenter: RouterCommandResponder {
    func respond(command: RouteCommand) -> Bool {
        if command is PaySuccessCommand {
            self.refresh()
        }
        return false
    }
}

extension MainPresenter: MainEventHandler {
	func bind(view: MainViewBehavior, router: MainRoutable) {
        self.view = view
        self.router = router
        self.router.responder = self
    }

    func didLoad() {
        self.view.set(mainItems: self.createItems())
        self.view.set(callItem: self.createCallItem())
        self.view.set(backImage: nil)
        self.view.set(accounts: [], selected: 0)
        self.loadAccounts(nil)

        self.accountService.accountsSequence()
            .subscribe(onNext: { [weak self] accounts in
                self?.items = accounts
                self?.currentIndex = self?.selectedIndex(from: accounts) ?? 0
                self?.view.set(accounts: accounts,
                               selected: self!.selectedIndex(from: accounts))
            })
            .disposed(by: self.bag)
    }

    func refresh() {
        let activity = self.view.showRefreshIndicator()
        self.loadAccounts(activity)
    }

    func select(account: Int) {
        if account >= self.items.count {
            return
        }
        self.currentIndex = account
        let value = self.items[account]
        self.accountService.select(account: value.id)

        self.view.set(callItem: self.createCallItem(value.phone))
        self.view.set(backImage: value.image)
        self.view.set(countersInfo: value.counters)
    }

    func pay() {
        if self.currentIndex >= self.items.count {
            return
        }
        let value = self.items[self.currentIndex]
        self.router.openAccountRefill(account: value)
    }

    func addAccount() {
        self.router.openAddAccount(chekAccounts: false, isFirst: false)
    }

    func openAccount() {
        if let value = self.currentAccount {
            self.router.openDetail(account: value)
        }
    }

    func profile() {
        self.router.openProfile()
    }

    func counters() {
        if self.currentIndex >= self.items.count {
            return
        }
        let value = self.items[self.currentIndex]
        self.router.openCounters(for: value)
    }

    func accruals() {
        if let value = self.currentAccount {
            self.router.openAccruals(for: value)
        }
    }

    func payments() {
        if let value = self.currentAccount {
            self.router.openPayments(for: value)
        }
    }

    func issues() {
        if let value = self.currentAccount {
            self.router.openIssueList(for: value)
        }
    }

    func contacts() {
        if let value = self.currentAccount {
            self.router.openContacts(for: value)
        }
    }
}

extension MainPresenter {
    private func createCallItem(_ phone: String = "") -> MainItem {
        var action: ActionFunc?
        if !phone.isEmpty {
            action = { [weak self] in self?.router.open(url: .phone(phone)) }
        }

        return MainItem(icon: Asset.phoneMainIcon.image,
                        title: L10n.Screen.Main.Item.Phone.title,
                        subtitle: phone.phoneWithAdditional(),
                        action: action)
    }

    private func loadAccounts(_ activity: ActivityDisposable?) {
        self.accountService
            .fetchAccounts()
            .manageActivity(activity)
            .subscribe(onError: self.defaultErrorHandling)
            .disposed(by: self.bag)
    }

    private func selectedIndex(from accounts: [Account]) -> Int {
        let id = self.accountService.selectedAccountId()
        return accounts.firstIndex(where: { $0.id == id }) ?? 0
    }

    private func createItems() -> [MainItem] {
        let strings = L10n.Screen.Main.Item.self
        return [
            MainItem(icon: Asset.counterMainIcon.image,
                     title: strings.Counter.title,
                     subtitle: strings.Counter.subtitle,
                     action: { [weak self] in self?.counters() }),
            MainItem(icon: Asset.listMainIcon.image,
                     title: strings.List.title,
                     subtitle: strings.List.subtitle,
                     action: { [weak self] in self?.accruals() }),
            MainItem(icon: Asset.payMainIcon.image,
                     title: strings.Pay.title,
                     subtitle: strings.Pay.subtitle,
                     action: { [weak self] in self?.payments() }),
            MainItem(icon: Asset.contactsMainIcon.image,
                     title: strings.Contacts.title,
                     subtitle: strings.Contacts.subtitle,
                     action: { [weak self] in self?.contacts() })
        ]
    }
}

import UIKit

// MARK: - Router

protocol MainRoutable: BaseRoutable,
    AlertRoute,
    AccountRefillRoute,
    UserProfileRoute,
    AppUrlRoute,
    CountersRoute,
    AddAccountRoute,
    AccrualsRoute,
    IssueListRoute,
    ContactsRoute,
    AccountDetailRoute,
    PaymentsRoute {}

final class MainRouter: BaseRouter, MainRoutable {}

package com.mintrocket.zhelezno.presentation.addressSelection.premise

import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import com.mintrocket.zhelezno.Screens
import com.mintrocket.zhelezno.entity.app.address.select.SelectBuilding
import com.mintrocket.zhelezno.entity.app.address.select.SelectHousingComplex
import com.mintrocket.zhelezno.model.interactor.address.AddressInteractor
import com.mintrocket.zhelezno.presentation.global.BasePresenter
import com.mintrocket.zhelezno.presentation.global.ErrorHandler
import com.mintrocket.zhelezno.ui.addressSelection.premise.SelectPremiseFragment
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class SelectBuildingsPresenter @Inject constructor(
        private val addressInteractor: AddressInteractor,
        private val router: Router,
        private val errorHandler: ErrorHandler
) : BasePresenter<SelectBuildingsView>() {

    private val currentItems = mutableListOf<SelectHousingComplex>()
    private var currentQuery = ""

    var resultScreen: String? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadBuildings()
    }

    fun loadBuildings() {
        addressInteractor
                .loadBuildings()
                .doOnSubscribe { viewState.setRefreshing(true) }
                .doAfterTerminate { viewState.setRefreshing(false) }
                .subscribe({
                    currentItems.clear()
                    currentItems.addAll(it)
                    updateItems()
                }, {
                    errorHandler.proceed(it)
                })
                .addToDisposable()
    }

    private fun updateItems() {
        val result = mutableListOf<SelectHousingComplex>()
        currentItems.forEach { complex ->
            val filteredBuilding = complex.buildings.filter { building ->
                building.address?.contains(currentQuery, true) == true
            }
            if (filteredBuilding.isNotEmpty()) {
                result.add(SelectHousingComplex(complex.id, complex.name, filteredBuilding))
            }
        }
        viewState.showHouses(result)
    }

    fun onSearchQueryChanged(query: String) {
        currentQuery = query
        updateItems()
    }

    fun onItemClick(item: SelectBuilding) {
        val complex = currentItems.find { it.buildings.firstOrNull { it.id == item.id } != null }
        router.replaceScreen(Screens.SELECT_PREMISE, Bundle().apply {
            putLong(SelectPremiseFragment.ARG_BUILDING_ID, item.id)
            putString(SelectPremiseFragment.ARG_BUILDING_TITLE, item.address)
            putString(SelectPremiseFragment.ARG_COMPLEX_TITLE, complex?.name)
            putString(SelectPremiseFragment.ARG_RESULT_SCREEN, resultScreen)
        })
    }

    fun onBackPressed() = router.exit()
}

package com.mintrocket.zhelezno.presentation.addressSelection

import com.arellomobile.mvp.InjectViewState
import com.mintrocket.zhelezno.Screens
import com.mintrocket.zhelezno.entity.app.address.Premise
import com.mintrocket.zhelezno.model.data.auth.AuthHolder
import com.mintrocket.zhelezno.model.interactor.address.AddressInteractor
import com.mintrocket.zhelezno.model.repository.intercom.IntercomRepository
import com.mintrocket.zhelezno.model.repository.settings.IntercomSettingsRepository
import com.mintrocket.zhelezno.presentation.global.BasePresenter
import com.mintrocket.zhelezno.presentation.global.ErrorHandler
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class AddressSelectionPresenter @Inject constructor(
        private val initParams: InitParams,
        private val addressInteractor: AddressInteractor,
        private val errorHandler: ErrorHandler,
        private val router: Router,
        private val intercomRepository: IntercomRepository,
        private val intercomSettingsRepository: IntercomSettingsRepository,
        private val authHolder: AuthHolder
) : BasePresenter<AddressSelectionView>() {

    data class InitParams(val mode: AddressSelectionMode)

    private var selectedItem: Premise? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadAddresses()
        loadAddresses(true)
    }

    fun onRefresh() {
        loadAddresses(true)
    }

    fun selectAddress() {
        selectedItem?.let { addressInteractor.selectAddress(it) }
        when (initParams.mode) {
            AddressSelectionMode.INITIAL -> router.newRootScreen(Screens.MAIN)
            AddressSelectionMode.SIMPLE -> router.exit()
        }
    }

    fun onAddressItemClick(item: Premise) {
        selectedItem = item
        selectAddress()
    }

    fun resetSelection() {
        selectedItem = null
    }

    private fun loadAddresses(force: Boolean = false) {
        addressInteractor
                .loadAddresses(force)
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe(
                        { viewState.showItems(it) },
                        { errorHandler.proceed(it) }
                )
                .addToDisposable()
    }
}

package com.mintrocket.zhelezno.ui.addressSelection.premise

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.mintrocket.zhelezno.R
import com.mintrocket.zhelezno.di.DI
import com.mintrocket.zhelezno.di.getDependency
import com.mintrocket.zhelezno.entity.app.address.select.SelectPremise
import com.mintrocket.zhelezno.extension.addTextChangeListener
import com.mintrocket.zhelezno.presentation.addressSelection.premise.SelectPremisePresenter
import com.mintrocket.zhelezno.presentation.addressSelection.premise.SelectPremiseView
import com.mintrocket.zhelezno.ui.global.BaseFragment
import com.mintrocket.zhelezno.ui.global.list.SelectPremiseListItem
import com.mintrocket.zhelezno.ui.global.list.ListItem
import com.mintrocket.zhelezno.ui.global.list.chat.create.SelectPremiseDelegate
import kotlinx.android.synthetic.main.fragment_select_md_premises.*

class SelectPremiseFragment : BaseFragment(), SelectPremiseView {

    companion object {
        const val ARG_BUILDING_ID = "building id"
        const val ARG_BUILDING_TITLE = "building title"
        const val ARG_COMPLEX_TITLE = "complex title"
        const val ARG_RESULT_SCREEN = "result screen"
    }

    override val layoutRes: Int = R.layout.fragment_select_md_premises

    private val adapter = ItemsAdapter()

    @InjectPresenter
    lateinit var presenter: SelectPremisePresenter

    @ProvidePresenter
    fun providePresenter(): SelectPremisePresenter =
            getDependency(SelectPremisePresenter::class.java, *screenScopes)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            presenter.buildingId = it.getLong(ARG_BUILDING_ID, presenter.buildingId)
            presenter.buildingTitle = it.getString(ARG_BUILDING_TITLE, presenter.buildingTitle)
            presenter.complexTitle = it.getString(ARG_COMPLEX_TITLE, presenter.complexTitle)
            presenter.resultScreen = it.getString(ARG_RESULT_SCREEN, presenter.resultScreen)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }
        toolbar.navigationIcon = ContextCompat.getDrawable(toolbar.context, R.drawable.ic_app_bar_back)

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@SelectPremiseFragment.adapter
        }

        swipeRefreshLayout.setOnRefreshListener { presenter.loadPremises() }


        searchField.addTextChangeListener {
            presenter.onSearchQueryChanged(it)
        }
    }

    override fun setTitle(text: String) {
        chatPremisesTitle.text = text
    }

    override fun setSubtitle(text: String) {
        chatPremisesSubtitle.text = text
    }

    override fun showPremises(items: List<SelectPremise>) {
        adapter.showItems(items)
    }

    override fun setRefreshing(isRefreshing: Boolean) {
        swipeRefreshLayout.isRefreshing = isRefreshing
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    inner class ItemsAdapter : ListDelegationAdapter<MutableList<ListItem>>() {

        private val clickListener = { item: SelectPremise ->
            presenter.onItemClick(item)
        }

        init {
            items = mutableListOf()
            delegatesManager.apply {
                addDelegate(SelectPremiseDelegate(clickListener))
            }
        }

        fun showItems(newItems: List<SelectPremise>) {
            items.clear()
            items.addAll(newItems.map { SelectPremiseListItem(it) })
            notifyDataSetChanged()
        }
    }
}

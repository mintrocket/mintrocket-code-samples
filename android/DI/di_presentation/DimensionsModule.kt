package com.mintrocket.presentation.di

import com.mintrocket.presentation.common.DimensionsProvider
import toothpick.config.Module

class DimensionsModule : Module() {
    init {
        bind(DimensionsProvider::class.java).singletonInScope()
    }
}
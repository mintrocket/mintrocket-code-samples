package com.mintrocket.presentation.presenter.account.info

import androidx.lifecycle.MutableLiveData
import com.mintrocket.domain.common.SchedulersProvider
import com.mintrocket.domain.entity.account.Account
import com.mintrocket.domain.entity.account.AccountDetail
import com.mintrocket.domain.repository.AccountRepository
import com.mintrocket.presentation.common.ActionLiveData
import com.mintrocket.presentation.common.ErrorHandler
import com.mintrocket.presentation.extension.formatRuble
import com.mintrocket.presentation.navigation.Screens
import com.mintrocket.presentation.presenter.BaseViewModel
import com.mintrocket.presentation.presenter.account.info.entity.AccountInfoState
import com.mintrocket.presentation.presenter.home.entity.HomeAccountItemState
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class AccountInfoViewModel @Inject constructor(
    private val accountRepository: AccountRepository,
    private val schedulersProvider: SchedulersProvider,
    private val errorHandler: ErrorHandler,
    private val router: Router
) : BaseViewModel() {

    private var currentAccount: Account? = null
    private var currentDetailAccount: AccountDetail? = null

    val accountInfoData = MutableLiveData<AccountInfoState>()
    val progressState = ActionLiveData<Boolean>()

    var argAccountId: Int = -1

    override fun onCreate() {
        super.onCreate()

        accountRepository
            .observeAccounts()
            .filter { it.any { it.id == argAccountId } }
            .map { it.first { it.id == argAccountId } }
            .lifeSubscribe({
                currentAccount = it
                fillData()
            }, {
                errorHandler.proceed(it)
            })

        accountRepository
            .observeDetailAccounts()
            .filter { it.any { it.id == argAccountId } }
            .map { it.first { it.id == argAccountId } }
            .lifeSubscribe({
                currentDetailAccount = it
                fillData()
            }, {
                errorHandler.proceed(it)
            })

        refresh()
    }

    override fun onResume() {
        super.onResume()
        refresh()
    }

    fun refresh() {
        accountRepository
            .getAccount(argAccountId)
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { progressState.value = true }
            .doFinally { progressState.value = false }
            .lifeSubscribe({}, {
                errorHandler.proceed(it)
            })
    }

    fun onPayClick() {
        val account = currentAccount ?: return
        val balance = account.balance
        val sumToPay = balance?.summToPay
        val formatted = if (sumToPay != 0.0) {
            String.format("%.2f", sumToPay)
        } else {
            null
        }
        router.navigateTo(Screens.Payment.Create(formatted))
    }

    fun onInvoicesClick() {
        router.navigateTo(Screens.Invoice.List())
    }

    fun onPaymentsClick() {
        router.navigateTo(Screens.Payment.List())
    }

    private fun fillData() {
        val account = currentAccount
        val detailAccount = currentDetailAccount


        val sumDesc = when {
            account?.balance?.summToPay != 0.0 -> "Сумма к оплате"
            account?.balance?.balance == 0.0 -> "У вас все оплачено"
            else -> "Баланс лицевого счета"
        }
        val action = when {
            account?.balance?.summToPay != 0.0 -> "Оплатить"
            else -> "Пополнить"
        }
        val sum = when {
            account?.balance?.summToPay != 0.0 -> detailAccount?.balance?.summToPay
            else -> detailAccount?.balance?.balance
        }

        accountInfoData.value = AccountInfoState(
            account?.id ?: 0,
            account?.image,
            (account?.accountName ?: account?.address) ?: "Моя квартира",
            account?.accountNumber?.let { "№$it" }.orEmpty(),
            detailAccount?.name.orEmpty(),
            account?.address.orEmpty(),
            (sum ?: 0.0).formatRuble(),
            sumDesc,
            action,
            "${(detailAccount?.area ?: "0").replace('.', ',')} м²",
            "${(detailAccount?.livingArea ?: "0").replace('.', ',')} м²",
            (detailAccount?.registeredPeople ?: 0).toString(),
            (detailAccount?.people ?: 0).toString()
        )
    }


}
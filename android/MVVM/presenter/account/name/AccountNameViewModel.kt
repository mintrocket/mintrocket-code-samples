package com.mintrocket.presentation.presenter.account.name

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mintrocket.domain.common.SchedulersProvider
import com.mintrocket.domain.entity.account.AccountAddException
import com.mintrocket.domain.entity.account.AccountSearchForm
import com.mintrocket.domain.entity.auth.AuthState
import com.mintrocket.domain.repository.AccountRepository
import com.mintrocket.presentation.R
import com.mintrocket.presentation.common.ActionLiveData
import com.mintrocket.presentation.common.ErrorHandler
import com.mintrocket.presentation.common.ResourceManager
import com.mintrocket.presentation.common.form.AppValidationsStore
import com.mintrocket.presentation.common.form.SimpleForm
import com.mintrocket.presentation.common.form.field.FormField
import com.mintrocket.presentation.common.form.field.InputFormField
import com.mintrocket.presentation.common.form.field.SelectFormField
import com.mintrocket.presentation.navigation.Screens
import com.mintrocket.presentation.presenter.BaseViewModel
import com.mintrocket.presentation.presenter.auth.other.DatePick
import io.reactivex.Single
import ru.terrakok.cicerone.Router
import timber.log.Timber
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class AccountNameViewModel @Inject constructor(
    private val accountRepository: AccountRepository,
    private val schedulersProvider: SchedulersProvider,
    private val router: Router,
    private val errorHandler: ErrorHandler,
    private val resourceManager: ResourceManager,
    private val validationsStore: AppValidationsStore
) : BaseViewModel() {


    companion object {
        const val FIELD_ACCOUNT_NAME = "account_name"
    }

    val formData = MutableLiveData<List<FormField>>()
    val buttonActive = MutableLiveData<Boolean>()
    val screenProgressState = ActionLiveData<Boolean>()

    val form = SimpleForm()

    var argAccountId: Int = -1

    init {
        form.initFields(
            listOf(
                InputFormField(
                    FIELD_ACCOUNT_NAME,
                    resourceManager.getString(R.string.form_field_account_name)
                )
            )
        )
        form.setValidation(FIELD_ACCOUNT_NAME, validationsStore.accountName)

        formData.value = form.getFields()

        form.observeFieldsChanges()
            .debounce(1000L, TimeUnit.MILLISECONDS)
            .observeOn(schedulersProvider.ui())
            .lifeSubscribe {
                formData.value = form.getFields()
            }

        form.observeInvalidFields()
            .debounce(100L, TimeUnit.MILLISECONDS)
            .observeOn(schedulersProvider.ui())
            .lifeSubscribe {
                buttonActive.value = it.isEmpty()
            }

        form.validateAll(validationsStore.paramsLive)
    }

    override fun onCreate() {
        super.onCreate()
        accountRepository
            .observeAccounts()
            .observeOn(schedulersProvider.ui())
            .lifeSubscribe({
                it.firstOrNull { it.id == argAccountId }?.also {
                    form.getField(FIELD_ACCOUNT_NAME)?.value = it.accountName
                    formData.value = form.getFields()
                    form.validateAll(validationsStore.paramsLive)
                }
            }, {
                errorHandler.proceed(it)
            })
    }

    fun onInputValueChanged(field: InputFormField, value: String) {
        Timber.d("onInputValueChanged cur=${field.value}, new=$value")
        if (field.value == value) {
            return
        }
        field.value = value
        form.validateAll(validationsStore.paramsLiveCheck)
    }

    fun onActionClick() {
        form.validateAll(validationsStore.paramsFull)
        if (form.getInvalidFields().isNotEmpty()) {
            return
        }

        val newName = form.getField(FIELD_ACCOUNT_NAME)?.value.orEmpty()
        accountRepository
            .updateAccountName(argAccountId, newName)
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { screenProgressState.value = true }
            .doFinally { screenProgressState.value = false }
            .lifeSubscribe({
                router.exit()
            }, {
                errorHandler.proceed(it)
            })
    }
}
package com.mintrocket.presentation.presenter.home.entity

import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes

data class HomeMenuItemState(
    val id: Int,
    @DrawableRes val icRes: Int,
    val title: String,
    val desc: String,
    val dotVisible: Boolean = false,
    val colored: Boolean = false
)
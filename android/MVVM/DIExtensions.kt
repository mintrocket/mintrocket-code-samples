package com.mintrocket.presentation.extension

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mintrocket.presentation.di.DI
import com.mintrocket.presentation.di.ScopeProvider
import toothpick.config.Module

fun Any.objectScopeName() = "${javaClass.simpleName}_${hashCode()}"

fun <T> Fragment.getDependency(clazz: Class<T>, vararg scopes: String): T = DI.get(clazz, *scopes)
fun <T> Activity.getDependency(clazz: Class<T>, vararg scopes: String): T = DI.get(clazz, *scopes)

fun Fragment.injectDependencies(vararg scopes: String) = DI.inject(this, *scopes)
fun Fragment.injectDependencies(modules: Array<out Module>, vararg scopes: String) =
    DI.inject(this, modules, *scopes)

fun Activity.injectDependencies(vararg scopes: String) = DI.inject(this, *scopes)
fun Activity.injectDependencies(modules: Array<out Module>, vararg scopes: String) =
    DI.inject(this, modules, *scopes)

fun Fragment.closeDependenciesScopes(vararg scopes: String) = DI.close(*scopes)
fun Activity.closeDependenciesScopes(vararg scopes: String) = DI.close(*scopes)

fun <T> Fragment.getScopedDependency(clazz: Class<T>): T = when (this) {
    is ScopeProvider -> getDependency(clazz, screenScope)
    else -> getDependency(clazz)
}

fun <T> FragmentActivity.getScopedDependency(clazz: Class<T>): T = when (this) {
    is ScopeProvider -> getDependency(clazz, screenScope)
    else -> getDependency(clazz)
}

fun <T : ViewModel> Fragment.viewModel(clazz: Class<T>): Lazy<T> = lazy {
    ViewModelProviders
        .of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T =
                this@viewModel.getScopedDependency(modelClass)
        })
        .get(clazz)
}

fun <T : ViewModel> Fragment.sharedViewModel(clazz: Class<T>): Lazy<T> = lazy {
    ViewModelProviders
        .of(requireActivity(), object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T =
                this@sharedViewModel.getScopedDependency(modelClass)
        })
        .get(clazz)
}

fun <T : ViewModel> FragmentActivity.viewModel(clazz: Class<T>): Lazy<T> = lazy {
    ViewModelProviders
        .of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T =
                this@viewModel.getScopedDependency(modelClass)
        })
        .get(clazz)
}

fun <T : Fragment> T.putScopeArgument(scope: String?): T {
    arguments = (arguments ?: Bundle()).apply {
        scope?.also {
            putString(ScopeProvider.ARG_PARENT_SCOPE, it)
        }
    }
    return this
}

fun Intent.putScopeArgument(scopesProvider: ScopeProvider?): Intent {
    scopesProvider?.screenScope?.also {
        putExtra(ScopeProvider.ARG_PARENT_SCOPE, it)
    }
    return this
}





package com.mintrocket.presentation.screen.account.create

import android.animation.Animator
import android.app.DatePickerDialog
import android.os.Bundle
import android.text.Html
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.transition.TransitionManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.mintrocket.presentation.R
import com.mintrocket.presentation.common.BackController
import com.mintrocket.presentation.common.DateTextWatcher
import com.mintrocket.presentation.common.DimensionHelper
import com.mintrocket.presentation.common.LinkMovementMethod
import com.mintrocket.presentation.common.form.field.FormField
import com.mintrocket.presentation.common.form.field.InputFormField
import com.mintrocket.presentation.extension.*
import com.mintrocket.presentation.presenter.account.create.AccountCreateViewModel
import com.mintrocket.presentation.presenter.account.create.entity.PolicyDialogState
import com.mintrocket.presentation.presenter.auth.other.DatePick
import com.mintrocket.presentation.screen.BaseAppFragment
import kotlinx.android.synthetic.main.fragment_account_create.*
import kotlinx.android.synthetic.main.fragment_account_policy.*
import kotlinx.android.synthetic.main.fragment_app_base.*
import kotlinx.android.synthetic.main.merge_account_privacy_bottomsheet.*
import kotlinx.android.synthetic.main.widget_form_input_text.view.*
import java.util.*
import javax.inject.Inject

class AccountCreateFragment : BaseAppFragment() {

    companion object {
        private const val ARG_JUST_ADD = "arg_just_add"

        fun newInstance(justAdd: Boolean = false) = AccountCreateFragment().putExtra {
            putBoolean(ARG_JUST_ADD, justAdd)
        }
    }

    private val viewModel by viewModel(AccountCreateViewModel::class.java)

    private val inputFieldValueListener = { field: InputFormField, value: String ->
        viewModel.onInputValueChanged(field, value)
    }

    @Inject
    lateinit var backController: BackController

    override val layoutResource: Int
        get() = R.layout.fragment_account_create

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            viewModel.argJustAdd = getBoolean(ARG_JUST_ADD, viewModel.argJustAdd)
        }
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initPrivacyBottomSheet()

        baseToolbar.setNavigationOnClickListener { viewModel.onBackPressed() }
        baseToolbar.setNavigationIcon(R.drawable.ic_back)
        baseToolbar.title = getString(R.string.screen_account_create_title)

        accountFieldLastName.valueListener = inputFieldValueListener
        accountFieldFirstName.valueListener = inputFieldValueListener
        accountFieldPatronymicName.valueListener = inputFieldValueListener
        accountFieldPassportId.valueListener = inputFieldValueListener
        with(accountFieldPassportDate) {
            fieldInput.addTextChangedListener(
                DateTextWatcher(accountFieldPassportDate.fieldInput) { text ->
                    accountFieldPassportDate.getField()
                        ?.let { inputFieldValueListener.invoke(it, text) }
                })
            setEndDateIcon(R.drawable.ic_calendar_black)
            setDrawableListener {
                viewModel.onDateFieldClick(it)
            }
        }
        accountFieldAccountId.valueListener = inputFieldValueListener
        accountFieldAccountName.valueListener = inputFieldValueListener

        accountFillProfileBtn.setOnClickListener {
            viewModel.onFillProfileClick()
        }

        accountActionBtn.setOnClickListener {
            activity?.hideKeyboard()
            viewModel.onActionClick()
        }

        viewModel.formData.subscribe {
            it.forEach { field ->
                setField(field)
            }
            TransitionManager.endTransitions(accountLayout)
            TransitionManager.beginDelayedTransition(accountLayout)
        }

        viewModel.buttonActive.subscribe {
            accountActionBtn.isEnabled = it
        }

        viewModel.progressState.subscribe {
            accountActionBtn.progressVisible = it
        }

        viewModel.pickDateAction.subscribe {
            showDatePicker(it)
        }

        viewModel.screenProgressState.subscribe {
            showProgress(it)
        }

        viewModel.acceptProgressState.subscribe {
            accountPolicyAcceptBtn.progressVisible = it
        }

        viewModel.showPolicyAction.subscribe {
            if (it != null) {
                setAccountPolicy(it)
            }
            showPrivacyBottomSheet(it != null)
        }

    }

    override fun onBackPressed(): Boolean {
        val behavior = BottomSheetBehavior.from(bottomSheet)
        if (behavior.state != BottomSheetBehavior.STATE_HIDDEN) {
            showPrivacyBottomSheet(false)
            return true
        }
        viewModel.onBackPressed()
        return true
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        val behavior = BottomSheetBehavior.from(bottomSheet)
        if (behavior.state == BottomSheetBehavior.STATE_HIDDEN) {
            bottomSheetShade.alpha = 0.0f
            bottomSheetShade.isGone = true
        } else {
            bottomSheetShade.alpha = 1.0f
            bottomSheetShade.isVisible = true
        }
    }

    private fun initPrivacyBottomSheet() {
        baseAppRoot ?: return
        baseAppRoot.inflate(R.layout.merge_account_privacy_bottomsheet, true)

        accountPolicyAcceptBtn.text = getString(R.string.screen_account_policy_accept)

        accountPolicyText.movementMethod = LinkMovementMethod({
            viewModel.onLinkClick(it)
            return@LinkMovementMethod true
        }, null)

        bottomSheetShade.setOnClickListener {
            showPrivacyBottomSheet(false)
        }

        accountPolicyAcceptBtn.setOnClickListener {
            activity?.hideKeyboard()
            viewModel.onAcceptPolicy()
        }

        accountPolicyDenyBtn.setOnClickListener {
            showPrivacyBottomSheet(false)
        }


        val behavior = BottomSheetBehavior.from(bottomSheet)
        var lastIsHidden: Boolean? = null
        behavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                bottomSheetShade ?: return
                val isHidden = newState == BottomSheetBehavior.STATE_HIDDEN
                if (isHidden == lastIsHidden) return
                lastIsHidden = isHidden
                if (isHidden) {
                    viewModel.onDenyPolicy()
                }
                bottomSheetShade
                    .animate()
                    .alpha(if (isHidden) 0f else 1f)
                    .setInterpolator(AccelerateDecelerateInterpolator())
                    .setDuration(150)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationRepeat(animation: Animator?) {
                        }

                        override fun onAnimationEnd(animation: Animator?) {
                            if (isHidden) {
                                bottomSheetShade.isGone = true
                            }
                        }

                        override fun onAnimationCancel(animation: Animator?) {
                        }

                        override fun onAnimationStart(animation: Animator?) {
                            if (!isHidden) {
                                bottomSheetShade.isVisible = true
                            }
                        }
                    })
                    .start()
            }
        })


        behavior.isHideable = true
        behavior.skipCollapsed = true
        behavior.isFitToContents = true
        behavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetShade.alpha = 0.0f
        bottomSheetShade.isGone = true
    }

    private fun setAccountPolicy(state: PolicyDialogState) {
        accountPolicyTitle?.text = Html.fromHtml(state.title)
        accountPolicyText?.text = Html.fromHtml(state.message)
    }

    private fun showPrivacyBottomSheet(show: Boolean) {
        val behavior = BottomSheetBehavior.from(bottomSheet)
        behavior.state = if (show) {
            BottomSheetBehavior.STATE_EXPANDED
        } else {
            BottomSheetBehavior.STATE_HIDDEN
        }
    }

    private fun setField(field: FormField) {
        when (field.key) {
            AccountCreateViewModel.FIELD_LAST_NAME -> {
                accountFieldLastName.setField(field as InputFormField)
            }
            AccountCreateViewModel.FIELD_FIRST_NAME -> {
                accountFieldFirstName.setField(field as InputFormField)
            }
            AccountCreateViewModel.FIELD_PATRONYMIC_NAME -> {
                accountFieldPatronymicName.setField(field as InputFormField)
            }
            AccountCreateViewModel.FIELD_PASSPORT_ID -> {
                accountFieldPassportId.setField(field as InputFormField)
            }
            AccountCreateViewModel.FIELD_PASSPORT_DATE -> {
                accountFieldPassportDate.setField(field as InputFormField)
            }
            AccountCreateViewModel.FIELD_ACCOUNT_ID -> {
                accountFieldAccountId.setField(field as InputFormField)
            }
            AccountCreateViewModel.FIELD_ACCOUNT_NAME -> {
                accountFieldAccountName.setField(field as InputFormField)
            }
        }
    }

    override fun updateDimens(dimensions: DimensionHelper.Dimensions) {
        super.updateDimens(dimensions)
        scrollView.updatePadding(bottom = dimensions.bottom)
        bottomSheet?.updatePadding(bottom = dimensions.bottom)
    }

    private fun showDatePicker(pick: DatePick) {
        val calendar = Calendar.getInstance().also {
            it.time = pick.current
        }
        val yearMax = Calendar.getInstance().get(Calendar.YEAR)
        val currentYear =
            if (calendar.get(Calendar.YEAR) > yearMax) yearMax else calendar.get(Calendar.YEAR)
        val currentMonth = calendar.get(Calendar.MONTH)
        val currentDay = calendar.get(Calendar.DAY_OF_MONTH)
        val dialog = DatePickerDialog(
            context!!,
            DatePickerDialog.OnDateSetListener { picker, year, month, day ->
                val newDate = Calendar.getInstance().also {
                    it.timeInMillis = 0
                    it.set(Calendar.YEAR, year)
                    it.set(Calendar.MONTH, month)
                    it.set(Calendar.DAY_OF_MONTH, day)
                }
                viewModel.onDateFieldChanged(pick.field, newDate.time)
                accountFieldPassportDate.fieldInput.setText(viewModel.getDateValue(newDate.time))
            },
            currentYear,
            currentMonth,
            currentDay
        )
        pick.minDate?.also {
            dialog.datePicker.minDate = it.time
        }
        pick.maxDate?.also {
            dialog.datePicker.maxDate = it.time
        }
        dialog.show()
    }

}
package com.mintrocket.presentation.presenter.home.entity

data class HomeAccountItemState(
    val id: Int,
    val name: String,
    val number: String,
    val sum: String,
    val sumDesc: String,
    val action: String,
    val hasDebt: Boolean,
    val loading: Boolean
)
import UIKit

public final class PageIndicatorView: UIView {
    private var currentIndex: Int = 0

    private let dotSpace: CGFloat = 6
    private let dotSize: CGFloat = 7
    private let maxDots: Int = 5
    private let dotScale: CGPoint = CGPoint(x: 0.7, y: 0.7)
    private let activeAlpha: CGFloat = 1
    private let inactiveAlpha: CGFloat = 0.3

    private var dotsCount: Int = 0
    private var selectedTag: Int = 0

    private var dotViews: [UIView] = []
    private var leftDot: UIView!
    private var rightDot: UIView!

    private var hasLeft: Bool = false
    private var hasRight: Bool = false

    func configure(_ count: Int, current: Int) {
        self.clear()

        self.dotsCount = count
        let dots = min(count, self.maxDots)

        let gaps = CGFloat(dots - 2) * dotSpace
        let dotsWidth = CGFloat(dots) * dotSize

        let width = gaps + dotsWidth
        let startPosition = (self.frame.width - width) / 2

        let newPosition = self.dotSize + self.dotSpace
        for dot in 0..<dots {
            let view = self.createView(positionX: startPosition + CGFloat(dot) * newPosition)
            view.tag = dot
            self.addSubview(view)
            self.dotViews.append(view)
        }

        self.leftDot = self.createView(positionX: startPosition)
        self.rightDot = self.createView(positionX: startPosition + CGFloat(dots - 1) * newPosition)
        self.leftDot.alpha = 0
        self.rightDot.alpha = 0

        self.addSubview(leftDot)
        self.addSubview(rightDot)

        let manyDots = self.dotsCount > self.maxDots

        self.hasLeft = current >= self.maxDots - 1 && manyDots
        self.hasRight = manyDots && current != self.dotsCount - 1
        if current >= self.maxDots - 1 {
            if current == self.dotsCount - 1 {
                self.selectedTag = maxDots - 1
            } else {
                self.selectedTag = maxDots - 2
            }
        } else {
            self.selectedTag = current
        }
        self.currentIndex = current
        self.updateState(old: current)
    }

    func set(index: Int) {
        let old = self.currentIndex
        self.currentIndex = index
        self.updateState(old: old)
    }

    private func clear() {
        self.subviews.forEach {
            $0.removeFromSuperview()
        }
        self.dotViews = []
    }

    private func updateState(old: Int) {
        if self.dotViews.isEmpty {
            return
        }

        self.dotViews[selectedTag].alpha = self.inactiveAlpha
        if old == self.currentIndex {
            self.identityState()
            self.dotViews[selectedTag].alpha = self.activeAlpha
            return
        }

        if old < self.currentIndex {
            self.selectedTag += 1
            self.dotViews[selectedTag].alpha = self.activeAlpha
            if selectedTag == self.maxDots - 1 && self.hasRight {
                self.rightAnimation()
            } else {
                self.identityState()
            }
        } else if old > self.currentIndex {
            self.selectedTag -= 1
            self.dotViews[selectedTag].alpha = self.activeAlpha
            if selectedTag == 0 && self.hasLeft {
                self.leftAnimation()
            } else {
                self.identityState()
            }
        }
    }

    private func identityState() {
        self.dotViews.forEach {
            $0.transform = .identity
        }
        if self.hasLeft {
            self.dotViews.first?.transform = CGAffineTransform(scaleX: self.dotScale.x, y: self.dotScale.y)
        }

        if self.hasRight {
            self.dotViews.last?.transform = CGAffineTransform(scaleX: self.dotScale.x, y: self.dotScale.y)
        }

        self.rightDot.alpha = 0
        self.leftDot.alpha = 0
    }

    private func rightAnimation() {
        self.hasLeft = true
        self.hasRight = self.currentIndex < self.dotsCount - 2
        self.rightDot.transform = CGAffineTransform(scaleX: self.dotScale.x,
                                                    y: self.dotScale.y)
        let newPosition = self.dotSize + self.dotSpace

        let animations: ActionFunc = {
            self.dotViews.forEach {
                if $0.tag == 0 || $0.tag == 1 {
                    return
                }
                $0.transform = CGAffineTransform(translationX: -newPosition, y: 0)
            }
            self.dotViews[0].alpha = 0
            self.dotViews[1].transform = CGAffineTransform(translationX: -newPosition, y: 0)
                .scaledBy(x: self.dotScale.x, y: self.dotScale.y)
            self.rightDot.alpha = self.inactiveAlpha

            if self.hasRight == false {
                self.rightDot.transform = .identity
            }
        }

        let completion: Action<Bool> = { _ in
            self.dotViews[self.selectedTag].alpha = self.inactiveAlpha
            self.selectedTag = self.maxDots - 2
            self.dotViews.first?.alpha = self.inactiveAlpha
            self.dotViews[self.selectedTag].alpha = self.activeAlpha
            self.identityState()
        }

        UIView.animateKeyframes(withDuration: 0.3,
                                delay: 0,
                                animations: animations,
                                completion: completion)

    }

    private func leftAnimation() {
        self.hasRight = true
        self.hasLeft = self.currentIndex > 1
        self.leftDot.transform = CGAffineTransform(scaleX: self.dotScale.x, y: self.dotScale.y)
        let newPosition = self.dotSize + self.dotSpace

        let animations: ActionFunc = {
            self.dotViews.forEach {
                if $0.tag == self.maxDots - 1 || $0.tag == self.maxDots - 2 {
                    return
                }
                $0.transform = CGAffineTransform(translationX: newPosition, y: 0)
            }
            self.dotViews[self.maxDots - 1].alpha = 0
            self.dotViews[self.maxDots - 2].transform =  CGAffineTransform(translationX: newPosition, y: 0)
                .scaledBy(x: self.dotScale.x, y: self.dotScale.y)
            self.leftDot.alpha = self.inactiveAlpha

            if self.hasLeft == false {
                self.leftDot.transform = .identity
            }
        }

        let completion: Action<Bool> = { _ in
            self.dotViews[self.selectedTag].alpha = self.inactiveAlpha
            self.selectedTag = 1
            self.dotViews.last?.alpha = self.inactiveAlpha
            self.dotViews[self.selectedTag].alpha = self.activeAlpha
            self.identityState()
        }

        UIView.animateKeyframes(withDuration: 0.3,
                                delay: 0,
                                animations: animations,
                                completion: completion)
     }

    private func createView(positionX: CGFloat) -> UIView {
        UIView(frame: CGRect(x: positionX, y: 0, width: self.dotSize, height: self.dotSize))
            .apply {
                $0.layer.cornerRadius = self.dotSize / 2
                $0.backgroundColor = .white
                $0.alpha = self.inactiveAlpha
            }
    }

}

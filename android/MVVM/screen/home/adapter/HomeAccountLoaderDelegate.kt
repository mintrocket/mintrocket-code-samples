package com.mintrocket.presentation.screen.home.adapter

import app.modules.recycler_screen.BaseAdapterDelegate
import com.mintrocket.presentation.R
import com.mintrocket.presentation.screen.home.entity.HomeAccountListItem
import com.mintrocket.presentation.screen.home.entity.HomeAccountLoaderListItem
import kotlinx.android.synthetic.main.item_account_home_loader.*

class HomeAccountLoaderDelegate : BaseAdapterDelegate<HomeAccountLoaderListItem>(
    R.layout.item_account_home_loader,
    HomeAccountLoaderListItem::class
)
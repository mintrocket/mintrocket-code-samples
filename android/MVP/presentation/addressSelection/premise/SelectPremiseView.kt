package com.mintrocket.zhelezno.presentation.addressSelection.premise

import com.mintrocket.zhelezno.presentation.global.BaseMvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.mintrocket.zhelezno.entity.app.address.select.SelectPremise

@StateStrategyType(AddToEndSingleStrategy::class)
interface SelectPremiseView : BaseMvpView {
    fun showPremises(items: List<SelectPremise>)
    fun setRefreshing(isRefreshing: Boolean)
    fun setTitle(text: String)
    fun setSubtitle(text: String)
}

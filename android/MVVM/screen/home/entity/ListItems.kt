package com.mintrocket.presentation.screen.home.entity

import app.modules.recycler_screen.ListItem
import com.mintrocket.presentation.presenter.home.entity.HomeAccountItemState
import com.mintrocket.presentation.presenter.home.entity.HomeMenuItemState

data class HomeMenuListItem(val item: HomeMenuItemState) : ListItem()
data class HomeMenuWideListItem(val item: HomeMenuItemState) : ListItem()
data class HomeAccountListItem(val item: HomeAccountItemState) : ListItem()
class HomeAccountLoaderListItem() : ListItem()
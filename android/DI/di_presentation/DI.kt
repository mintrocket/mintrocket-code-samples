package com.mintrocket.presentation.di

import toothpick.Scope
import toothpick.Toothpick
import toothpick.config.Module

object DI {
    const val APP_SCOPE = "app_scope"

    fun <T> get(clazz: Class<T>, vararg scopes: String): T = openScopes(*scopes).getInstance(clazz)

    fun inject(target: Any, vararg scopes: String) = Toothpick.inject(target,
        openScopes(*scopes)
    )
    fun inject(target: Any, modules: Array<out Module>, vararg scopes: String) = Toothpick
        .inject(target, openScopes(*scopes).withModules(*modules))

    fun Scope.withModules(vararg modules: Module) = apply { installModules(*modules) }

    // Open APP_SCOPE and more
    fun openScopes(vararg scopes: String): Scope = if (scopes.isEmpty()) {
        Toothpick.openScopes(APP_SCOPE)
    } else {
        Toothpick.openScopes(*scopes)
    }

    fun close(vararg scopes: String) = scopes.forEach { Toothpick.closeScope(it) }
}
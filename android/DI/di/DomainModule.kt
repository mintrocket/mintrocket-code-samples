package ru.ingrad.home.di

import com.mintrocket.domain.interactor.AuthInteractor
import toothpick.config.Module

class DomainModule : Module() {

    init {
        bind(AuthInteractor::class.java).singletonInScope()
    }

}
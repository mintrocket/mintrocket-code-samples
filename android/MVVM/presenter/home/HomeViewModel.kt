package com.mintrocket.presentation.presenter.home

import androidx.lifecycle.MutableLiveData
import com.mintrocket.domain.common.SchedulersProvider
import com.mintrocket.domain.entity.account.Account
import com.mintrocket.domain.entity.account.AccountsMetaInfo
import com.mintrocket.domain.entity.auth.AuthState
import com.mintrocket.domain.entity.counter.CounterParams
import com.mintrocket.domain.interactor.AuthInteractor
import com.mintrocket.domain.repository.AccountRepository
import com.mintrocket.presentation.R
import com.mintrocket.presentation.common.ActionLiveData
import com.mintrocket.presentation.common.ErrorHandler
import com.mintrocket.presentation.common.intent.IntentHelper
import com.mintrocket.presentation.extension.formatRuble
import com.mintrocket.presentation.navigation.Screens
import com.mintrocket.presentation.presenter.BaseViewModel
import com.mintrocket.presentation.presenter.home.entity.HomeAccountItemState
import com.mintrocket.presentation.presenter.home.entity.HomeMenuItemState
import io.reactivex.Single
import ru.terrakok.cicerone.Router
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val accountRepository: AccountRepository,
    private val authInteractor: AuthInteractor,
    private val schedulersProvider: SchedulersProvider,
    private val router: Router,
    private val errorHandler: ErrorHandler,
    private val intentHelper: IntentHelper
) : BaseViewModel() {

    companion object {
        const val MENU_COUNTERS = 1
        const val MENU_PERIODS = 2
        const val MENU_PAYMENTS = 3
        const val MENU_BIDS = 4
        const val MENU_CONTACTS = 5
        const val MENU_CALL = 6
    }

    val menuStateData = MutableLiveData<List<HomeMenuItemState>>()
    val accountStateData = MutableLiveData<List<HomeAccountItemState>>()
    val currentHomeImage = MutableLiveData<String>()
    val currentAccountIndex = MutableLiveData<Int>()
    val alertData = MutableLiveData<String?>()
    val refreshAction = ActionLiveData<Boolean>()

    private var activeAccountData: AccountsMetaInfo? = null
    private val accountsList = mutableListOf<Account>()

    init {

        accountRepository
            .observeAccountMetaInfo()
            .distinctUntilChanged()
            .lifeSubscribe({
                activeAccountData = it
                updateScreenState()
            }, {
                errorHandler.proceed(it)
            })

        accountRepository
            .observeAccounts()
            .distinctUntilChanged()
            .lifeSubscribe({
                accountsList.clear()
                accountsList.addAll(it)
                updateScreenState()
            }, {
                errorHandler.proceed(it)
            })

        updateMenuItems()
    }

    override fun onResume() {
        super.onResume()

        accountRepository
            .getAccounts()
            .flatMap { accounts ->
                if (!accounts.any { it.policy }) {
                    authInteractor.setAuthProgress(AuthState.Progress.ACCOUNT).map { accounts }
                } else {
                    Single.just(accounts)
                }
            }
            .observeOn(schedulersProvider.ui())
            .lifeSubscribe({}, {
                errorHandler.proceed(it)
            })
    }

    fun refresh() {
        accountRepository
            .getAccounts()
            .flatMap { accounts ->
                if (!accounts.any { it.policy }) {
                    authInteractor.setAuthProgress(AuthState.Progress.ACCOUNT).map { accounts }
                } else {
                    Single.just(accounts)
                }
            }
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { refreshAction.value = true }
            .doFinally { refreshAction.value = false }
            .lifeSubscribe({}, {
                errorHandler.proceed(it)
            })
    }

    fun onAccountIndexChanged(index: Int) {
        val accountData = activeAccountData ?: return
        val id = accountsList.getOrNull(index)?.id ?: return
        if (accountData.activeId == id) return
        activeAccountData = accountData.copy(activeId = id)
        updateScreenState()
    }

    fun onAccountClick(item: HomeAccountItemState) {
        accountRepository.setSelectAccountInfo(AccountsMetaInfo(item.id))
            .subscribeOn(schedulersProvider.io())
            .lifeSubscribe({
                router.navigateTo(Screens.Account.Info(item.id))
            }, {
                errorHandler.proceed(it)
            })

    }

    fun onAddAccountClick() {
        router.navigateTo(Screens.Account.Create(true))
    }

    fun onAccountActionClick(item: HomeAccountItemState) {
        val balance = accountsList.firstOrNull { it.id == item.id }?.balance
        val sumToPay = balance?.summToPay
        val formatted = if (sumToPay != 0.0) {
            String.format("%.2f", sumToPay)
        } else {
            null
        }
        router.navigateTo(Screens.Payment.Create(formatted))
    }

    fun onAlertClick() {
        router.navigateTo(Screens.Counter.List())
    }

    fun onProfileClick() {
        router.navigateTo(Screens.Profile.Detail())
    }

    fun onMenuItemClick(item: HomeMenuItemState) {
        when (item.id) {
            MENU_COUNTERS -> router.navigateTo(Screens.Counter.List())
            MENU_PERIODS -> router.navigateTo(Screens.Invoice.List())
            MENU_PAYMENTS -> router.navigateTo(Screens.Payment.List())
            MENU_BIDS -> router.navigateTo(Screens.Bid.List())
            MENU_CONTACTS -> router.navigateTo(Screens.Contact())
            MENU_CALL -> intentHelper.intentPhone(item.desc)
        }
    }

    private fun updateScreenState() {
        val activeAccount = activeAccountData ?: return
        var activeAccountIndex = accountsList.indexOfFirst { it.id == activeAccount.activeId }
        if (accountsList.isEmpty() || activeAccountIndex == -1) {
            accountStateData.value = (0 until activeAccount.savedSize).map {
                HomeAccountItemState(-it, "", "", "", "", "", false, true)
            }
            currentAccountIndex.value = activeAccount.activeIndex
        }
        if (activeAccountIndex == -1 && accountsList.isNotEmpty()) {
            activeAccountIndex = 0
        }
        if (activeAccountIndex != -1 && accountsList.isNotEmpty()) {
            val newAccountInfo = activeAccount.copy(
                accountsList[activeAccountIndex].id,
                activeIndex = activeAccountIndex,
                savedSize = accountsList.size
            )
            if (activeAccount != newAccountInfo) {
                accountRepository
                    .setAccountMetaInfo(newAccountInfo)
                    .subscribeOn(schedulersProvider.io())
                    .lifeSubscribe({}, {
                        errorHandler.proceed(it)
                    })
            }

            updateAccountItems()
            val foundActiveAccount = accountsList[activeAccountIndex]
            currentAccountIndex.value = accountsList.indexOf(foundActiveAccount)
            currentHomeImage.value = foundActiveAccount.image.orEmpty()
            alertData.value = getAlertMessage(foundActiveAccount.countersParams)
            updateMenuItems()
        }


    }

    private fun getAlertMessage(params: CounterParams?): String? {
        if (params == null) return null

        val dateFormated =
            SimpleDateFormat("dd MMMM").format(Calendar.getInstance().let {
                it.set(Calendar.DAY_OF_MONTH, params.endDate)
                it.time
            })
        return if (params.canUpdate) {
            "Не забудьте передать показания счётчиков до $dateFormated"
        } else {
            null
        }
    }

    private fun updateAccountItems() {

        accountStateData.value = accountsList.map {

            val sumDesc = when {
                it.balance?.summToPay != 0.0 -> "Сумма к оплате"
                it.balance?.balance == 0.0 -> "У вас все оплачено"
                else -> "Баланс лицевого счета"
            }
            val action = when {
                it.balance?.summToPay != 0.0 -> "Оплатить"
                else -> "Пополнить"
            }
            val sum = when {
                it.balance?.summToPay != 0.0 -> it.balance?.summToPay
                else -> it.balance?.balance
            }

            val hasDebt = it.balance?.summToPay != 0.0

            HomeAccountItemState(
                it.id,
                (it.accountName ?: it.address) ?: "Моя квартира",
                "№${it.accountNumber}",
                (sum ?: 0.0).formatRuble(),
                sumDesc,
                action,
                hasDebt,
                false
            )
        }
    }

    private fun updateMenuItems() {
        val menu = mutableListOf(
            HomeMenuItemState(
                MENU_COUNTERS,
                R.drawable.ic_home_counters,
                "Счётчики",
                "Данные приборов"
            ),
            HomeMenuItemState(
                MENU_PERIODS,
                R.drawable.ic_home_periods,
                "Начисления",
                "По месяцам"
            ),
            HomeMenuItemState(
                MENU_PAYMENTS,
                R.drawable.ic_home_payments,
                "Платежи",
                "История платежей"
            ),
            /*HomeMenuItemState(
                MENU_BIDS,
                R.drawable.ic_home_bids,
                "Заявки в УК",
                "Статусы заявок"
            ),*/
            HomeMenuItemState(
                MENU_CONTACTS,
                R.drawable.ic_home_contacts,
                "Контакты",
                "Адреса и телефоны"
            )
        )

        val activeAccount = accountsList.firstOrNull { it.id == activeAccountData?.activeId }
        val phone = activeAccount?.phone.orEmpty().replace(",", " доб. ")
        if (phone.isNotEmpty()) {
            menu.add(
                HomeMenuItemState(
                    MENU_CALL,
                    R.drawable.ic_home_call,
                    "Позвонить диспетчеру",
                    phone,
                    colored = true
                )
            )
        }
        menuStateData.value = menu
    }
}
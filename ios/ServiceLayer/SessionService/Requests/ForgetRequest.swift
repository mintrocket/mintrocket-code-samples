import UIKit.UIDevice

public struct ForgetRequest: BackendAPIRequest {
    typealias ResponseObject = PhoneCheckResult

    private(set) var endpoint: String = "auth/forget"
    private(set) var method: NetworkManager.Method = .POST
    private(set) var parameters: [String: Any]
    private(set) var headers: [String: String] = [:]

    init(phone: String) {
        let deviceId = UIDevice.deviceId

        self.parameters = [
            "device_id": deviceId,
            "phone": phone
        ]

        if let token = TokenFactory.create(phone: phone, deviceId: deviceId) {
            self.headers = [
                "Authorization": token
            ]
        }
    }
}

package com.mintrocket.presentation.screen.account.list.entity

import app.modules.recycler_screen.ListItem
import com.mintrocket.presentation.presenter.account.list.entity.AccountState

data class AccountListItem(val item: AccountState) : ListItem()
import UIKit

public final class MainItemView: LoadableView {
    @IBOutlet private var stackView: UIStackView!
    @IBOutlet private weak var backView: UIView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var subtitleLabel: UILabel!
    @IBOutlet private var iconView: UIImageView!

    private var item: MainItem?

    public func configure(_ item: MainItem) {
        self.item = item
        self.iconView.image = item.icon
        self.titleLabel.text = item.title
        self.subtitleLabel.text = item.subtitle
    }

    @IBAction func itemTapped(_ sender: Any) {
        self.item?.action?()
    }

    @IBInspectable
    var isHorizontal: Bool = false {
        didSet {
            self.stackView.axis = isHorizontal ? .horizontal : .vertical
        }
    }

    @IBInspectable
    var backColor: UIColor? {
        didSet {
            self.backView.backgroundColor = self.backColor
        }
    }

    @IBInspectable
    var titleTextColor: UIColor? {
        get {
            return self.titleLabel.textColor
        }
        set {
            self.titleLabel.textColor = newValue
        }
    }

    @IBInspectable
    var subtitleTextColor: UIColor? {
        get {
            return self.subtitleLabel.textColor
        }
        set {
            self.subtitleLabel.textColor = newValue
        }
    }
}

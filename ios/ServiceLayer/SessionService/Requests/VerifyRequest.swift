import UIKit.UIDevice

public struct VerifyRequest: BackendAPIRequest {
    typealias ResponseObject = SessionData

    private(set) var endpoint: String = "auth/verify"
    private(set) var method: NetworkManager.Method = .POST
    private(set) var parameters: [String: Any]
    private(set) var headers: [String: String] = [:]

    init(code: String, uuid: String) {
        let deviceId = UIDevice.deviceId

        self.parameters = [
            "device_id": deviceId,
            "code": code,
            "uuid": uuid
        ]
        if let token = TokenFactory.create(uuid: uuid) {
            self.headers = [
                "Authorization": token
            ]
        }
    }
}

package ru.ingrad.home.di

import android.content.Context
import ru.ingrad.home.common.AppSchedulers
import com.mintrocket.domain.common.SchedulersProvider
import toothpick.config.Module

class AppModule(context: Context) : Module() {

    init {
        bind(Context::class.java).toInstance(context)
        bind(SchedulersProvider::class.java).to(AppSchedulers::class.java).singletonInScope()
    }

}
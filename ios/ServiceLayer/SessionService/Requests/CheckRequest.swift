import UIKit.UIDevice

public struct CheckRequest: BackendAPIRequest {
    typealias ResponseObject = PhoneCheckResult

    private(set) var endpoint: String = "auth/check"
    private(set) var method: NetworkManager.Method = .POST
    private(set) var parameters: [String: Any]
    private(set) var headers: [String: String] = [:]

    init(phone: String) {
        let deviceId = UIDevice.deviceId

        self.parameters = [
            "app_version": Bundle.main.releaseVersionNumber ?? "",
            "device_id": deviceId,
            "model": UIDevice.model,
            "os_version": UIDevice.current.systemVersion,
            "platform": "ios",
            "phone": phone
        ]

        if let token = TokenFactory.create(phone: phone, deviceId: deviceId) {
            self.headers = [
                "Authorization": token
            ]
        }
    }
}

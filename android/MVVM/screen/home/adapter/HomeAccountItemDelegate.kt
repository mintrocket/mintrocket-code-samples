package com.mintrocket.presentation.screen.home.adapter

import android.transition.TransitionManager
import android.util.Log
import android.view.ViewGroup
import android.view.animation.Animation
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import app.modules.recycler_screen.BaseAdapterDelegate
import com.mintrocket.presentation.R
import com.mintrocket.presentation.extension.getColorFromAttr
import com.mintrocket.presentation.screen.home.entity.HomeAccountListItem
import kotlinx.android.synthetic.main.item_account_home.*

class HomeAccountItemDelegate(
    private val clickListener: (HomeAccountListItem) -> Unit,
    private val onAddClick: (HomeAccountListItem) -> Unit,
    private val onActionClick: (HomeAccountListItem) -> Unit
) : BaseAdapterDelegate<HomeAccountListItem>(
    R.layout.item_account_home,
    HomeAccountListItem::class
) {

    override fun bindData(item: HomeAccountListItem, holder: BaseViewHolder<HomeAccountListItem>) {
        super.bindData(item, holder)
        val data = item.item
        holder.apply {
            itemAccountName.text = data.name
            itemAccountNumber.text = data.number
            itemAccountSum.text = data.sum
            itemAccountSumDesc.text = data.sumDesc
            itemAccountPayBtn.text = data.action
            itemAccountAddBtn.setOnClickListener { onAddClick.invoke(item) }
            itemAccountPayBtn.setOnClickListener { onActionClick.invoke(item) }
            itemAccountSumDesc.setTextColor(
                if (data.hasDebt) {
                    itemAccountSumDesc.context.getColorFromAttr(R.attr.paletteRed)
                } else {
                    itemAccountSumDesc.context.getColorFromAttr(R.attr.textSecond)
                }
            )
            itemAccountInfo.isInvisible = data.loading
            progressBar.isVisible = data.loading
            containerView.setOnClickListener { clickListener.invoke(item) }
        }
    }

    override fun applyPayloads(
        item: HomeAccountListItem,
        holder: BaseViewHolder<HomeAccountListItem>,
        payloads: List<Any>
    ) {
        super.applyPayloads(item, holder, payloads)
        bindData(item, holder)
    }

}
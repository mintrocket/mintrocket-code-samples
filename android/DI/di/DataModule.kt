package ru.ingrad.home.di

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mintrocket.data.api.ApiConfig
import com.mintrocket.data.api.ApiEndpointHolder
import com.mintrocket.data.api.interceptor.AuthTokenHolder
import com.mintrocket.data.api.interceptor.AuthTokenInterceptor
import com.mintrocket.data.api.interceptor.JsonHeadersInterceptor
import com.mintrocket.data.api.service.account.AccountApi
import com.mintrocket.data.api.service.auth.AuthApi
import com.mintrocket.data.api.service.bid.BidApi
import com.mintrocket.data.api.service.contact.ContactApi
import com.mintrocket.data.api.service.counter.CounterApi
import com.mintrocket.data.api.service.invoice.InvoiceApi
import com.mintrocket.data.api.service.news.NewsApi
import com.mintrocket.data.api.service.peyment.PaymentApi
import com.mintrocket.data.api.service.profile.ProfileApi
import com.mintrocket.data.common.DataErrorHandler
import com.mintrocket.data.common.encryption.EncryptionKeyStoreHelper
import com.mintrocket.data.common.encryption.KeyStoreHelper
import com.mintrocket.data.repository.account.AccountRepositoryImpl
import com.mintrocket.data.repository.auth.AuthRepositoryImpl
import com.mintrocket.data.repository.bid.BidRepositoryImpl
import com.mintrocket.data.repository.contact.ContactRepositoryImpl
import com.mintrocket.data.repository.counter.CounterRepositoryImpl
import com.mintrocket.data.repository.document.DocumentRepositoryImpl
import com.mintrocket.data.repository.imagepicker.ImagePickerRepositoryImpl
import com.mintrocket.data.repository.invoice.InvoiceRepositoryImpl
import com.mintrocket.data.repository.news.NewsRepositoryImpl
import com.mintrocket.data.repository.other.TempFileRepositoryImpl
import com.mintrocket.data.repository.payment.PaymentRepositoryImpl
import com.mintrocket.data.repository.profile.ProfileRepositoryImpl
import com.mintrocket.data.storage.account.AccountStorage
import com.mintrocket.data.storage.account.AccountStorageImpl
import com.mintrocket.data.storage.auth.AuthStorage
import com.mintrocket.data.storage.auth.AuthStorageImpl
import com.mintrocket.data.storage.news.NewsStorage
import com.mintrocket.domain.common.IDataErrorHandler
import com.mintrocket.domain.common.SchedulersProvider
import com.mintrocket.domain.repository.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.ingrad.home.BuildConfig
import toothpick.config.Module
import javax.inject.Inject
import javax.inject.Provider

class DataModule(context: Context) : Module() {

    init {

        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        bind(SharedPreferences::class.java).toInstance(sharedPreferences)

        bind(IDataErrorHandler::class.java).to(DataErrorHandler::class.java).singletonInScope()
        bind(KeyStoreHelper::class.java).to(EncryptionKeyStoreHelper::class.java).singletonInScope()


        bind(AuthStorageImpl::class.java).singletonInScope()
        bind(AuthStorage::class.java).to(AuthStorageImpl::class.java).singletonInScope()
        bind(AccountStorage::class.java).to(AccountStorageImpl::class.java).singletonInScope()


        bind(ApiEndpointHolder::class.java).to(ApiConfig::class.java).singletonInScope()
        bind(AuthTokenHolder::class.java).to(AuthStorageImpl::class.java).singletonInScope()

        bind(NewsStorage::class.java).singletonInScope()


        bind(Gson::class.java).toProvider(GsonProvider::class.java).providesSingletonInScope()
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java)
            .providesSingletonInScope()
        bind(Retrofit::class.java).toProvider(RetrofitProvider::class.java)
            .providesSingletonInScope()


        bind(AuthApi::class.java)
            .toProvider(AuthApiProvider::class.java)
            .providesSingletonInScope()

        bind(ProfileApi::class.java)
            .toProvider(ProfileApiProvider::class.java)
            .providesSingletonInScope()

        bind(AccountApi::class.java)
            .toProvider(AccountApiProvider::class.java)
            .providesSingletonInScope()

        bind(CounterApi::class.java)
            .toProvider(CounterApiProvider::class.java)
            .providesSingletonInScope()

        bind(InvoiceApi::class.java)
            .toProvider(InvoiceApiProvider::class.java)
            .providesSingletonInScope()

        bind(PaymentApi::class.java)
            .toProvider(PaymentApiProvider::class.java)
            .providesSingletonInScope()

        bind(ContactApi::class.java)
            .toProvider(ContactApiProvider::class.java)
            .providesSingletonInScope()

        bind(BidApi::class.java)
            .toProvider(BidApiProvider::class.java)
            .providesSingletonInScope()

        bind(NewsApi::class.java)
            .toProvider(NewsApiProvider::class.java)
            .providesSingletonInScope()

        bind(AuthRepository::class.java).to(AuthRepositoryImpl::class.java).singletonInScope()
        bind(ProfileRepository::class.java).to(ProfileRepositoryImpl::class.java).singletonInScope()
        bind(AccountRepository::class.java).to(AccountRepositoryImpl::class.java).singletonInScope()
        bind(CounterRepository::class.java).to(CounterRepositoryImpl::class.java).singletonInScope()
        bind(InvoiceRepository::class.java).to(InvoiceRepositoryImpl::class.java).singletonInScope()
        bind(TempFileRepository::class.java).to(TempFileRepositoryImpl::class.java)
            .singletonInScope()
        bind(PaymentRepository::class.java).to(PaymentRepositoryImpl::class.java).singletonInScope()
        bind(DocumentRepository::class.java).to(DocumentRepositoryImpl::class.java)
            .singletonInScope()
        bind(ContactRepository::class.java).to(ContactRepositoryImpl::class.java).singletonInScope()
        bind(BidRepository::class.java).to(BidRepositoryImpl::class.java).singletonInScope()
        bind(ImagePickerRepository::class.java).to(ImagePickerRepositoryImpl::class.java)
            .singletonInScope()
        bind(NewsRepository::class.java).to(NewsRepositoryImpl::class.java).singletonInScope()
    }


    /* Network part */
    internal class GsonProvider @Inject constructor() : Provider<Gson> {
        override fun get(): Gson = GsonBuilder().create()
    }

    internal class OkHttpClientProvider @Inject constructor(
        private val tokenInterceptor: AuthTokenInterceptor
    ) : Provider<OkHttpClient> {

        override fun get(): OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(JsonHeadersInterceptor())
            .addInterceptor(tokenInterceptor)
            .addInterceptor(
                HttpLoggingInterceptor().setLevel(
                    if (BuildConfig.DEBUG) {
                        HttpLoggingInterceptor.Level.BODY
                    } else {
                        HttpLoggingInterceptor.Level.NONE
                    }
                )
            )
            .build()
    }

    internal class RetrofitProvider @Inject constructor(
        private val okHttpClient: OkHttpClient,
        private val gson: Gson,
        private val apiEndpointHolder: ApiEndpointHolder,
        private val schedulersProvider: SchedulersProvider
    ) : Provider<Retrofit> {

        override fun get(): Retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(apiEndpointHolder.getApiEndpoint())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(schedulersProvider.io()))
            .build()
    }

    /* Retrofit api part */
    /* Да, на каждый интерфейс по провайдеру. Страх. Нужно просто принять это. */
    internal class AuthApiProvider @Inject constructor(
        private val retrofit: Retrofit
    ) : Provider<AuthApi> {

        override fun get(): AuthApi = retrofit.create(AuthApi::class.java)
    }

    internal class ProfileApiProvider @Inject constructor(
        private val retrofit: Retrofit
    ) : Provider<ProfileApi> {

        override fun get(): ProfileApi = retrofit.create(ProfileApi::class.java)
    }

    internal class AccountApiProvider @Inject constructor(
        private val retrofit: Retrofit
    ) : Provider<AccountApi> {

        override fun get(): AccountApi = retrofit.create(AccountApi::class.java)
    }

    internal class CounterApiProvider @Inject constructor(
        private val retrofit: Retrofit
    ) : Provider<CounterApi> {

        override fun get(): CounterApi = retrofit.create(CounterApi::class.java)
    }

    internal class InvoiceApiProvider @Inject constructor(
        private val retrofit: Retrofit
    ) : Provider<InvoiceApi> {

        override fun get(): InvoiceApi = retrofit.create(InvoiceApi::class.java)
    }

    internal class PaymentApiProvider @Inject constructor(
        private val retrofit: Retrofit
    ) : Provider<PaymentApi> {

        override fun get(): PaymentApi = retrofit.create(PaymentApi::class.java)
    }

    internal class ContactApiProvider @Inject constructor(
        private val retrofit: Retrofit
    ) : Provider<ContactApi> {

        override fun get(): ContactApi = retrofit.create(ContactApi::class.java)
    }

    internal class BidApiProvider @Inject constructor(
        private val retrofit: Retrofit
    ) : Provider<BidApi> {

        override fun get(): BidApi = retrofit.create(BidApi::class.java)
    }

    internal class NewsApiProvider @Inject constructor(
        private val retrofit: Retrofit
    ) : Provider<NewsApi> {
        override fun get(): NewsApi = retrofit.create(NewsApi::class.java)
    }

}
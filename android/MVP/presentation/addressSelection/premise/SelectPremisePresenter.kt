package com.mintrocket.zhelezno.presentation.addressSelection.premise

import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import com.mintrocket.zhelezno.Screens
import com.mintrocket.zhelezno.entity.app.address.select.SelectPremise
import com.mintrocket.zhelezno.entity.app.address.select.PremiseSelectResult
import com.mintrocket.zhelezno.model.interactor.address.AddressInteractor
import com.mintrocket.zhelezno.model.interactor.chat.ChatCreateInteractor
import com.mintrocket.zhelezno.presentation.RequestCodes
import com.mintrocket.zhelezno.presentation.global.BasePresenter
import com.mintrocket.zhelezno.presentation.global.ErrorHandler
import com.mintrocket.zhelezno.ui.addressSelection.premise.SelectBuildingsFragment
import com.mintrocket.zhelezno.ui.chat.detail.ChatFragment
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class SelectPremisePresenter @Inject constructor(
        private val chatCreateInteractor: ChatCreateInteractor,
        private val addressInteractor: AddressInteractor,
        private val router: Router,
        private val errorHandler: ErrorHandler
) : BasePresenter<SelectPremiseView>() {

    private val currentItems = mutableListOf<SelectPremise>()
    private var currentQuery = ""

    var buildingId = 0L
    var buildingTitle: String? = null
    var complexTitle: String? = null

    var resultScreen: String? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.setTitle(buildingTitle.orEmpty())
        viewState.setSubtitle(complexTitle.orEmpty())
        loadPremises()
    }

    fun loadPremises() {
        addressInteractor
                .loadPremises(buildingId)
                .doOnSubscribe { viewState.setRefreshing(true) }
                .doAfterTerminate { viewState.setRefreshing(false) }
                .subscribe({
                    currentItems.clear()
                    currentItems.addAll(it)
                    updateItems()
                }, {
                    errorHandler.proceed(it)
                })
                .addToDisposable()
    }

    private fun updateItems() {
        val result = currentItems.filter { premise ->
            premise.premiseNum?.contains(currentQuery, true) == true
        }
        viewState.showPremises(result)
    }

    fun onSearchQueryChanged(query: String) {
        currentQuery = query
        updateItems()
    }

    fun onItemClick(item: SelectPremise) {
        when (resultScreen) {
            Screens.CHAT_DETAIL -> {
                chatCreateInteractor
                        .createChat(item.id)
                        .subscribe({ chat ->
                            router.newScreenChain(Screens.CHAT_DETAIL, Bundle().apply {
                                putLong(ChatFragment.ARG_ID, chat?.id ?: 0L)
                                putString(ChatFragment.ARG_TITLE, chat?.name)
                            })
                        }, {
                            errorHandler.proceed(it)
                        })
                        .addToDisposable()
            }
            else -> {
                router.exitWithResult(RequestCodes.PREMISE_SELECT, PremiseSelectResult(
                        item.id,
                        item.name,
                        item.premiseNum,
                        buildingId,
                        buildingTitle,
                        complexTitle
                ))
            }
        }
    }

    fun onBackPressed() = router.replaceScreen(Screens.SELECT_HOUSE, Bundle().apply {
        putString(SelectBuildingsFragment.ARG_RESULT_SCREEN, resultScreen)
    })
}

import UIKit

public protocol SideViewDelegate: class {
    func add()
    func pay()
    func card()
}

public final class SlideView: LoadableView {
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var sumLabel: UILabel!
    @IBOutlet var sumTitleLabel: UILabel!
    @IBOutlet var payButton: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet var loadIndicatorView: UIView!
    @IBOutlet var contentView: UIView!

    weak var delegate: SideViewDelegate?

    private var isEnabled: Bool = true {
        didSet {
            if isEnabled {
                self.rippleManager.rippleColor = MainTheme.shared.black
            } else {
                self.rippleManager.rippleColor = nil
            }
        }
    }

    private lazy var rippleManager: RippleManager = { [unowned self] in
        RippleManager(attatch: self)
    }()

    @IBOutlet var rippleContainerView: UIView! {
        didSet {
            self.rippleManager.attach(to: self.rippleContainerView)
        }
    }

    private static let moneyFormatter: Formatting = FormatterFactory.money([.grouping]).create()

    public var isActive: Bool = false {
        didSet {
            if self.isActive {
                self.backView.backgroundColor = self.activeColor
            } else {
                self.backView.backgroundColor = self.inactiveColor
            }
        }
    }

    public var isLoading: Bool = false {
        didSet {
            self.contentView.isHidden = self.isLoading
            self.loadIndicatorView.isHidden = !self.isLoading
        }
    }

    public override func setupNib() {
        super.setupNib()
        if UIScreen.main.isSmall {
            self.stackView.axis = .vertical
        }
    }

    private let activeColor: UIColor = MainTheme.shared.yellow
    private let inactiveColor: UIColor = MainTheme.shared.white.withAlphaComponent(0.3)

    func configure(_ value: Account, enabled: Bool) {
        self.isEnabled = enabled

        self.titleLabel.text = value.actualName
        self.numberLabel.text = "№\(value.number)"
        if let balance = value.balance {
            var title = L10n.Screen.Main.sumTitle
            var buttonText = L10n.Buttons.pay
            var color = MainTheme.shared.red

            var sum = balance.summToPay
            if sum?.doubleValue == 0 {
                sum = balance.value
                buttonText = L10n.Buttons.refill
                color = MainTheme.shared.black.withAlphaComponent(0.5)
                if sum?.doubleValue == 0 {
                    title = L10n.Screen.Main.balanceEmpty
                } else {
                    title = L10n.Screen.Main.balanceNonEmpty
                }
            }

            UIView.performWithoutAnimation {
                self.payButton.setTitle(buttonText, for: .normal)
                self.payButton.layoutIfNeeded()
            }
            self.sumLabel.text = Self.moneyFormatter.string(from: sum)
            self.sumTitleLabel.text = title
            self.sumTitleLabel.textColor = color
        } else {
            self.sumLabel.text = ""
            self.sumTitleLabel.text = ""
        }
    }

    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.rippleManager.touchesBegan(touches)
    }

    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.rippleManager.touchesEnded()

        if self.isEnabled {
            self.delegate?.card()
        }
    }

    public override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        self.rippleManager.touchesEnded()
    }

    @IBAction func addTapped(_ sender: Any) {
        self.delegate?.add()
    }

    @IBAction func payTapped(_ sender: Any) {
        self.delegate?.pay()
    }

    func state(progress: CGFloat) {
        var fromColor = self.inactiveColor
        var toColor = self.activeColor
        if self.isActive {
            fromColor = self.activeColor
            toColor = self.inactiveColor
        }

        self.backView.backgroundColor = fromColor.interpolate(with: toColor,
                                                              friction: progress)
    }
}

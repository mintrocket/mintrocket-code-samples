package com.mintrocket.zhelezno.presentation.addressSelection

enum class AddressSelectionMode {
    INITIAL,
    SIMPLE
}

import UIKit.UIDevice

public struct LoginRequest: BackendAPIRequest {
    typealias ResponseObject = SessionData

    private(set) var endpoint: String = "auth/login"
    private(set) var method: NetworkManager.Method = .POST
    private(set) var parameters: [String: Any]
    private(set) var headers: [String: String] = [:]

    init(phone: String, password: String) {
        let deviceId = UIDevice.deviceId

        self.parameters = [
            "device_id": deviceId,
            "username": phone,
            "password": password
        ]
        if let token = TokenFactory.create(phone: phone, deviceId: deviceId) {
            self.headers = [
                "Authorization": token
            ]
        }
    }
}

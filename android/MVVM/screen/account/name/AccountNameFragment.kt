package com.mintrocket.presentation.screen.account.name

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AlertDialogLayout
import androidx.transition.TransitionManager
import com.mintrocket.presentation.R
import com.mintrocket.presentation.common.form.field.InputFormField
import com.mintrocket.presentation.extension.putExtra
import com.mintrocket.presentation.extension.showKeyboard
import com.mintrocket.presentation.extension.showProgress
import com.mintrocket.presentation.extension.viewModel
import com.mintrocket.presentation.presenter.account.name.AccountNameViewModel
import com.mintrocket.presentation.screen.BaseDialogFragment
import kotlinx.android.synthetic.main.dialog_account_name.*
import kotlinx.android.synthetic.main.dialog_account_name.view.*
import kotlinx.android.synthetic.main.widget_form_input_text.view.*

class AccountNameFragment : BaseDialogFragment() {

    companion object {
        private const val ARG_ID = "arg_id"

        fun newInstance(id: Int) = AccountNameFragment().putExtra {
            putInt(ARG_ID, id)
        }
    }

    private val viewModel by viewModel(AccountNameViewModel::class.java)

    private var screenLayout: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            viewModel.argAccountId = getInt(ARG_ID, viewModel.argAccountId)
        }
        lifecycle.addObserver(viewModel)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val layout = LayoutInflater
            .from(context!!)
            .inflate(R.layout.dialog_account_name, null, false)
        screenLayout = layout

        return AlertDialog.Builder(context!!)
            .setTitle("Изменить название")
            .setView(layout)
            .setPositiveButton("Изменить") { dialog, which ->
            }
            .setNegativeButton("Отмена", null)
            .create()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = screenLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        accountNameFieldName.valueListener = { field: InputFormField, value: String ->
            viewModel.onInputValueChanged(field, value)
        }

        viewModel.formData.subscribe {
            it.forEach { field ->
                when (field.key) {
                    AccountNameViewModel.FIELD_ACCOUNT_NAME -> {
                        accountNameFieldName.setField(field as InputFormField)
                    }
                }
            }
        }

        viewModel.buttonActive.subscribe {
            actionButton?.isEnabled = it
        }

        viewModel.screenProgressState.subscribe {
            showProgress(it)
        }
    }

    override fun onStart() {
        super.onStart()
        actionButton?.setOnClickListener {
            viewModel.onActionClick()
        }

        accountNameFieldName.requestFocus()
        accountNameFieldName.post{
            accountNameFieldName.fieldInput.showKeyboard(activity)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        screenLayout = null
    }

    private val actionButton: Button?
        get() = (dialog as AlertDialog?)?.getButton(AlertDialog.BUTTON_POSITIVE)
}
package com.mintrocket.presentation.screen.account.info

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.core.graphics.ColorUtils
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import com.mintrocket.presentation.R
import com.mintrocket.presentation.common.BackController
import com.mintrocket.presentation.common.DimensionHelper
import com.mintrocket.presentation.extension.putExtra
import com.mintrocket.presentation.extension.viewModel
import com.mintrocket.presentation.presenter.account.info.AccountInfoViewModel
import com.mintrocket.presentation.screen.BaseScreenFragment
import com.nostra13.universalimageloader.core.ImageLoader
import kotlinx.android.synthetic.main.fragment_account_info.*
import javax.inject.Inject

class AccountInfoFragment : BaseScreenFragment() {

    companion object {
        private const val ARG_ID = "arg_id"

        fun newInstance(id: Int) = AccountInfoFragment().putExtra {
            putInt(ARG_ID, id)
        }
    }

    private val viewModel by viewModel(AccountInfoViewModel::class.java)

    @Inject
    lateinit var backController: BackController

    override val layoutResource: Int
        get() = R.layout.fragment_account_info

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            viewModel.argAccountId = getInt(ARG_ID, viewModel.argAccountId)
        }
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val color = Color.parseColor("#000000")
        val toolbarBg = ColorDrawable(ColorUtils.setAlphaComponent(color, 0))
        accountToolbar.background = toolbarBg

        accountToolbar.setNavigationOnClickListener { backController.back() }

        accountSwipeRefresh.setOnRefreshListener { viewModel.refresh() }

        accountSumPayBtn.setOnClickListener { viewModel.onPayClick() }
        accountInvoicesBtn.setOnClickListener { viewModel.onInvoicesClick() }
        accountPaymentsBtn.setOnClickListener { viewModel.onPaymentsClick() }


        // Затенение статусбара
        accountScrollView.setOnScrollChangeListener { v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            accountToolbar ?: return@setOnScrollChangeListener
            v ?: return@setOnScrollChangeListener

            val percent = (scrollY / accountToolbar.height.toFloat()).coerceIn(0f, 1f)
            val newColor = ColorUtils.setAlphaComponent(color, (percent * 255 * 0.2).toInt())
            if (toolbarBg.color != newColor) {
                toolbarBg.color = newColor
            }
        }

        viewModel.progressState.subscribe {
            accountSwipeRefresh.isRefreshing = it
        }

        viewModel.accountInfoData.subscribe {
            accountName.text = it.name
            accountNumber.text = it.number
            accountUserName.text = it.userName
            accountAddress.text = it.address

            accountSum.text = it.sum
            accountSumDesc.text = it.sumDesc
            accountSumPayBtn.text = it.payBtn

            accountInfoArea.text = Html.fromHtml(it.totalArea)
            accountInfoLiveArea.text = Html.fromHtml(it.liveArea)
            accountInfoRegPeople.text = it.registeredPeople
            accountInfoPeople.text = it.livePeople

            if (it.image.isNullOrEmpty()) {
                accountImage.setImageResource(R.drawable.image)
            } else {
                ImageLoader.getInstance().displayImage(it.image, accountImage)
            }
        }

    }

    override fun isLightStatusBar(): Boolean {
        return false
    }

    override fun updateDimens(dimensions: DimensionHelper.Dimensions) {
        super.updateDimens(dimensions)
        accountToolbar.updatePadding(top = dimensions.top)
        accountInfoStats.updatePadding(bottom = dimensions.bottom)
    }

}
package com.mintrocket.presentation.di

import android.content.Context
import com.mintrocket.presentation.common.ResourceManager
import com.mintrocket.presentation.common.form.AppValidationsStore
import com.mintrocket.presentation.common.intent.IntentHelper
import com.mintrocket.presentation.common.intent.IntentHelperImpl
import toothpick.config.Module

class ResourceModule(context: Context) : Module() {
    init {
        bind(ResourceManager::class.java).toInstance(ResourceManager(context))
        bind(AppValidationsStore::class.java).singletonInScope()
        bind(IntentHelper::class.java).toInstance(IntentHelperImpl(context))
    }
}
package com.mintrocket.presentation.presenter.account.list.entity

data class AccountState(
    val id: Int,
    val name: String,
    val number: String,
    val address: String
)
package com.mintrocket.presentation.screen.account.list.adapter

import app.modules.recycler_screen.BaseAdapterDelegate
import com.mintrocket.presentation.R
import com.mintrocket.presentation.screen.account.list.entity.AccountListItem
import kotlinx.android.synthetic.main.item_account.*

class AccountDelegate(
    private val clickListener: (AccountListItem) -> Unit,
    private val editClickListener: (AccountListItem) -> Unit,
    private val deleteClickListener: (AccountListItem) -> Unit
) : BaseAdapterDelegate<AccountListItem>(
    R.layout.item_account,
    AccountListItem::class
) {
    override fun bindData(item: AccountListItem, holder: BaseViewHolder<AccountListItem>) {
        super.bindData(item, holder)
        val data = item.item
        holder.apply {
            itemAccountName.text = data.name
            itemAccountNumber.text = data.number
            itemAccountAddress.text = data.address
            itemAccount.setOnClickListener { clickListener.invoke(item) }
            itemAccountEditBtn.setOnClickListener { editClickListener.invoke(item) }
            itemAccountDeleteBtn.setOnClickListener { deleteClickListener.invoke(item) }
        }
    }

    override fun applyPayloads(
        item: AccountListItem,
        holder: BaseViewHolder<AccountListItem>,
        payloads: List<Any>
    ) {
        super.applyPayloads(item, holder, payloads)
        bindData(item, holder)
    }
}
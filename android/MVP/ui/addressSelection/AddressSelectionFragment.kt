package com.mintrocket.zhelezno.ui.addressSelection

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.mintrocket.zhelezno.R
import com.mintrocket.zhelezno.di.DI
import com.mintrocket.zhelezno.di.getDependency
import com.mintrocket.zhelezno.di.injectDependencies
import com.mintrocket.zhelezno.entity.app.address.Premise
import com.mintrocket.zhelezno.presentation.addressSelection.AddressSelectionMode
import com.mintrocket.zhelezno.presentation.addressSelection.AddressSelectionPresenter
import com.mintrocket.zhelezno.presentation.addressSelection.AddressSelectionView
import com.mintrocket.zhelezno.ui.global.BaseFragment
import com.mintrocket.zhelezno.ui.global.list.AddressListItem
import com.mintrocket.zhelezno.ui.global.list.ListItem
import com.mintrocket.zhelezno.ui.global.list.addressSelection.AddressItemAdapterDelegate
import kotlinx.android.synthetic.main.fragment_address_selection.*
import toothpick.config.Module


class AddressSelectionFragment : BaseFragment(), AddressSelectionView, AppBarLayout.OnOffsetChangedListener {

    companion object {
        private const val ARG_MODE = "mode"

        fun newInstance(mode: AddressSelectionMode) = AddressSelectionFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARG_MODE, mode)
            }
        }
    }

    override val localModules: Array<Module>
        get() = arrayOf(object : Module() {
            init {
                val mode = arguments?.getSerializable(ARG_MODE) as? AddressSelectionMode
                        ?: AddressSelectionMode.INITIAL
                val params = AddressSelectionPresenter.InitParams(mode)
                bind(AddressSelectionPresenter.InitParams::class.java).toInstance(params)
            }
        })

    override val layoutRes = R.layout.fragment_address_selection

    private val adapter = ItemsAdapter()

    @InjectPresenter
    lateinit var presenter: AddressSelectionPresenter

    @ProvidePresenter
    fun providePresenter(): AddressSelectionPresenter = getDependency(AddressSelectionPresenter::class.java, *screenScopes)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@AddressSelectionFragment.adapter
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        }
        swipeRefreshLayout.setOnRefreshListener { presenter.onRefresh() }
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout, i: Int) {
        swipeRefreshLayout.isEnabled = i == 0
    }

    override fun showProgress(isVisible: Boolean) {
        swipeRefreshLayout.isRefreshing = isVisible
    }

    override fun showItems(items: List<Premise>) {
        adapter.showItems(items)
    }

    override fun selectAddressItem(item: Premise) {
        adapter.selectItem(item)
    }

    inner class ItemsAdapter : ListDelegationAdapter<MutableList<ListItem>>() {

        private var selectedItemId: Long = 0

        private val clickListener = { item: Premise ->
            presenter.onAddressItemClick(item)
        }

        init {
            items = mutableListOf()
            delegatesManager.run {
                addDelegate(AddressItemAdapterDelegate(clickListener))
            }
        }

        fun showItems(addressItems: List<Premise>) {
            items.clear()
            items.addAll(addressItems.map { AddressListItem(it, it.premiseId == selectedItemId) })

            if (items.isEmpty()) {
                presenter.resetSelection()
            }

            notifyDataSetChanged()
        }

        fun selectItem(item: Premise) {
            items.forEachIndexed { index, listItem ->
                (listItem as? AddressListItem)?.let {
                    it.isSelected = it.item.premiseId == item.premiseId
                    notifyItemChanged(index)
                }
            }

            selectedItemId = item.premiseId
        }
    }
}

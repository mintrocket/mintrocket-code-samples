import UIKit

// MARK: - View Controller

final class MainViewController: BaseViewController {
    @IBOutlet var mainItemConstraints: [NSLayoutConstraint]?
    @IBOutlet var mainItemViews: [MainItemView]!
    @IBOutlet var callItemView: MainItemView!
    @IBOutlet var accountImageView: UIImageView!
    @IBOutlet var sliderView: SliderView!
    @IBOutlet var pageIndicatorView: PageIndicatorView!
    @IBOutlet var remainderView: UIView!
    @IBOutlet var remainderLabel: UILabel!

    var handler: MainEventHandler!

    private var needHideBar: Bool = false

    override func initialize() {
        super.initialize()
        self.statusBarStyle = .lightContent
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateItemConstraints()
        self.pageIndicatorView.isHidden = true
        self.setupSlider()
        self.addRefreshControl()
        self.handler.didLoad()
    }

    override func refresh() {
        super.refresh()
        self.handler.refresh()
    }

    private func updateItemConstraints() {
        if UIScreen.main.isLarge {
            self.mainItemConstraints?.forEach {
                $0.constant = 150
            }
        }
    }

    private func setupSlider() {
        self.sliderView.indexDidChange { [weak self] index in
            self?.handler.select(account: index)
            self?.pageIndicatorView.set(index: index)
        }

        self.sliderView.pay { [weak self] in
            self?.handler.pay()
        }

        self.sliderView.add { [weak self] in
            self?.handler.addAccount()
        }

        self.sliderView.tapped { [weak self] in
            self?.needHideBar = true
            self?.handler.openAccount()
        }
    }

    func pageIndicator(show: Bool) {
        if self.pageIndicatorView.isHidden == !show {
            return
        }
        self.pageIndicatorView.isHidden = !show
        UIView.animate(withDuration: 0.2) {
            UIApplication.shared.keyWindow?.layoutIfNeeded()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.needHideBar {
            self.needHideBar = false
            return
        }
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    // MARK: - Actions

    @IBAction func profileTapped(_ sender: Any) {
        self.handler.profile()
    }

    @IBAction func countersTapped(_ sender: Any) {
        self.handler.counters()
    }

}

extension MainViewController: MainViewBehavior {
    func set(mainItems: [MainItem]) {
        self.mainItemViews.enumerated().forEach {
            $0.element.configure(mainItems[$0.offset])
        }
    }

    func set(callItem: MainItem) {
        self.callItemView.isHidden = callItem.subtitle.isEmpty
        self.callItemView.configure(callItem)
        self.callItemView.fadeTransition()
    }

    func set(backImage: URL?) {
        self.accountImageView.setImage(from: backImage,
                                       placeholder: Asset.accountPlaceholder.image)
        self.accountImageView.fadeTransition()
    }

    func set(countersInfo: CountersInfo?) {
        let old = self.remainderView.isHidden
        var hidden = self.remainderView.isHidden

        defer {
            if old != hidden {
                if hidden == false {
                    self.remainderView.isHidden = false
                    UIView.animate(withDuration: 0.3) {
                        self.remainderView.alpha = 1
                        UIApplication.shared.keyWindow?.layoutIfNeeded()
                    }
                } else {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.remainderView.alpha = 0
                    }, completion: { _ in
                        self.remainderView.isHidden = true
                        UIView.animate(withDuration: 0.3) {
                            UIApplication.shared.keyWindow?.layoutIfNeeded()
                        }
                    })
                }
            }
        }

        guard let range = countersInfo?.dayRange else {
            hidden = true
            return
        }

        if countersInfo?.canUpdate == false {
            hidden = true
            return
        }

        let messageBuilder = AttributeStringBuilder()
            .set(font: UIFont.font(ofSize: 15, weight: .regular))
            .set(lineHeight: 20)
            .set(color: MainTheme.shared.white)

        let day = range.max() ?? 0

        self.remainderLabel.attributedText = messageBuilder
            .build(L10n.Screen.Main.sendMeterReminder("\(day) \(Date().monthName)"))
        hidden = false
    }

    func set(accounts: [Account], selected: Int) {
        if accounts.count > 1 {
            self.pageIndicator(show: true)
            self.pageIndicatorView.configure(accounts.count, current: selected)
        } else {
           self.pageIndicator(show: false)
        }
        self.sliderView.configure(items: accounts, current: selected)

        if accounts.isEmpty {
            self.callItemView.isHidden = true
        } else {
            self.callItemView.isHidden = false
            self.callItemView.fadeTransition()
        }
    }
}

package com.mintrocket.zhelezno.presentation.addressSelection

import com.mintrocket.zhelezno.presentation.global.BaseMvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.mintrocket.zhelezno.entity.app.address.Premise

@StateStrategyType(AddToEndSingleStrategy::class)
interface AddressSelectionView : BaseMvpView {
    fun showProgress(isVisible: Boolean)
    fun showItems(items: List<Premise>)
    fun selectAddressItem(item: Premise)
}

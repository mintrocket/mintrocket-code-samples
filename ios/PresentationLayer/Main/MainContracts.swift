import UIKit

// MARK: - Contracts

protocol MainViewBehavior: WaitingBehavior, RefreshBehavior {
    func set(mainItems: [MainItem])
    func set(callItem: MainItem)
    func set(countersInfo: CountersInfo?)
    func set(backImage: URL?)
    func set(accounts: [Account], selected: Int)
}

protocol MainEventHandler: ViewControllerEventHandler, RefreshEventHandler {
    func bind(view: MainViewBehavior, router: MainRoutable)

    func profile()
    func addAccount()
    func openAccount()
    func pay()
    func select(account: Int)

    func counters()
}

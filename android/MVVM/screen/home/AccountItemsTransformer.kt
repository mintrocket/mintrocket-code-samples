package com.mintrocket.presentation.screen.home

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.View
import androidx.cardview.widget.CardView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.animation.ArgbEvaluatorCompat
import com.mintrocket.presentation.R
import com.mintrocket.presentation.extension.getCompatColor
import kotlin.math.abs

class AccountItemsTransformer(val context: Context) : ViewPager2.PageTransformer {

    private val evaluator = ArgbEvaluatorCompat()
    private val bgColor = context.getCompatColor(R.color.light_homeAccount)
    private val targetColor = context.getCompatColor(R.color.light_homeAccountSelected)

    override fun transformPage(page: View, position: Float) {
        page as CardView
        val progress = abs(position).coerceIn(0f, 1f)
        page.setCardBackgroundColor(evaluator.evaluate(progress, targetColor, bgColor))
    }
}
package com.mintrocket.presentation.presenter.account.create

import androidx.lifecycle.MutableLiveData
import com.mintrocket.domain.common.SchedulersProvider
import com.mintrocket.domain.entity.account.AccountAddException
import com.mintrocket.domain.entity.account.AccountSearchForm
import com.mintrocket.domain.entity.account.PolicyRequire
import com.mintrocket.domain.entity.auth.AuthState
import com.mintrocket.domain.interactor.AuthInteractor
import com.mintrocket.domain.repository.AccountRepository
import com.mintrocket.domain.repository.ProfileRepository
import com.mintrocket.presentation.R
import com.mintrocket.presentation.common.ActionLiveData
import com.mintrocket.presentation.common.ErrorHandler
import com.mintrocket.presentation.common.ResourceManager
import com.mintrocket.presentation.common.form.AppValidationsStore
import com.mintrocket.presentation.common.form.SimpleForm
import com.mintrocket.presentation.common.form.field.FormField
import com.mintrocket.presentation.common.form.field.InputFormField
import com.mintrocket.presentation.extension.getDateOrNull
import com.mintrocket.presentation.navigation.Screens
import com.mintrocket.presentation.presenter.BaseViewModel
import com.mintrocket.presentation.presenter.account.create.entity.PolicyDialogState
import com.mintrocket.presentation.presenter.auth.other.DatePick
import io.reactivex.Single
import ru.terrakok.cicerone.Router
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class AccountCreateViewModel @Inject constructor(
    private val resourceManager: ResourceManager,
    private val schedulersProvider: SchedulersProvider,
    private val validationsStore: AppValidationsStore,
    private val router: Router,
    private val authInteractor: AuthInteractor,
    private val accountRepository: AccountRepository,
    private val profileRepository: ProfileRepository,
    private val errorHandler: ErrorHandler
) : BaseViewModel() {

    companion object {
        const val FIELD_LAST_NAME = "lastname"
        const val FIELD_FIRST_NAME = "firstname"
        const val FIELD_PATRONYMIC_NAME = "patornymicname"

        const val FIELD_PASSPORT_ID = "passport_id"
        const val FIELD_PASSPORT_DATE = "passport_date"

        const val FIELD_ACCOUNT_ID = "account_id"
        const val FIELD_ACCOUNT_NAME = "account_name"
    }

    private val formatter = SimpleDateFormat("ddMMyyyy")

    private var currentPolicyRequire: PolicyRequire? = null

    val formData = MutableLiveData<List<FormField>>()
    val buttonActive = MutableLiveData<Boolean>()
    val progressState = ActionLiveData<Boolean>()
    val acceptProgressState = ActionLiveData<Boolean>()
    val screenProgressState = ActionLiveData<Boolean>()

    val pickDateAction = ActionLiveData<DatePick>()
    val showPolicyAction = MutableLiveData<PolicyDialogState?>()

    val form = SimpleForm()

    var argJustAdd: Boolean = false

    init {
        showPolicyAction.value = null
        form.initFields(
            listOf(
                InputFormField(
                    FIELD_LAST_NAME,
                    resourceManager.getString(R.string.form_field_last_name)
                ),
                InputFormField(
                    FIELD_FIRST_NAME,
                    resourceManager.getString(R.string.form_field_first_name)
                ),
                InputFormField(
                    FIELD_PATRONYMIC_NAME,
                    resourceManager.getString(R.string.form_field_patronymic_name),
                    necessary = false
                ),
                InputFormField(
                    FIELD_PASSPORT_ID,
                    resourceManager.getString(R.string.form_field_passport_id),
                    inputType = InputFormField.InputType.NUMBER
                ),
                InputFormField(
                    FIELD_PASSPORT_DATE,
                    resourceManager.getString(R.string.form_field_passport_date),
                    inputType = InputFormField.InputType.DATE
                ),
                InputFormField(
                    FIELD_ACCOUNT_ID,
                    resourceManager.getString(R.string.form_field_account_id)
                ).also {
                    it.helperText = "Указан в квитанции"
                },
                InputFormField(
                    FIELD_ACCOUNT_NAME,
                    resourceManager.getString(R.string.form_field_account_name),
                    necessary = false
                ).also {
                    it.helperText = "Например, Моя квартира"
                }
            )
        )
        form.setValidation(FIELD_LAST_NAME, validationsStore.name)
        form.setValidation(FIELD_FIRST_NAME, validationsStore.name)
        form.setValidation(FIELD_PATRONYMIC_NAME, validationsStore.name)
        form.setValidation(FIELD_PASSPORT_ID, validationsStore.empty)
        form.setValidation(FIELD_PASSPORT_DATE, validationsStore.select)
        form.setValidation(FIELD_ACCOUNT_ID, validationsStore.empty)
        form.setValidation(FIELD_ACCOUNT_NAME, validationsStore.accountName)
        form.setValidation(FIELD_PASSPORT_DATE, validationsStore.date)

        formData.value = form.getFields()

        form.observeFieldsChanges()
            .debounce(1000L, TimeUnit.MILLISECONDS)
            .observeOn(schedulersProvider.ui())
            .lifeSubscribe {
                formData.value = form.getFields()
            }

        form.observeInvalidFields()
            .debounce(100L, TimeUnit.MILLISECONDS)
            .observeOn(schedulersProvider.ui())
            .lifeSubscribe {
                buttonActive.value = it.isEmpty()
            }

        form.validateAll(validationsStore.paramsLive)
    }

    override fun onCreate() {
        super.onCreate()

        accountRepository
            .getAccounts()
            .flatMap { accounts ->
                if (!accounts.any { it.policy } || argJustAdd) {
                    Single.just(accounts)
                } else {
                    authInteractor.setAuthProgress(AuthState.Progress.COMPLETE).map { accounts }
                }
            }
            .observeOn(schedulersProvider.ui())
            /*.doOnSubscribe { screenProgressState.value = true }
            .doFinally { screenProgressState.value = false }*/
            .lifeSubscribe({

            }, {
                errorHandler.proceed(it)
            })
    }

    fun onInputValueChanged(field: InputFormField, value: String) {
        Timber.d("onInputValueChanged cur=${field.value}, new=$value")
        if (field.value == value) {
            return
        }
        field.value = value
        form.validateAll(validationsStore.paramsLiveCheck)
    }

    fun onActionClick() {
        form.validateAll(validationsStore.paramsFull)

        val formRequest = generateFormRequest() ?: return
        accountRepository
            .searchAccount(formRequest)
            .flatMap { accounts ->
                if (argJustAdd) {
                    Single.just(accounts)
                } else {
                    authInteractor.setAuthProgress(AuthState.Progress.COMPLETE).map { accounts }
                }
            }
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { progressState.value = true }
            .doFinally { progressState.value = false }
            .lifeSubscribe({
                if (argJustAdd) {
                    router.exit()
                }
            }, {
                handleAddAccountError(it)
            })
    }

    fun onAcceptPolicy() {
        form.validateAll(validationsStore.paramsFull)

        val formRequest = generateFormRequest() ?: return
        formRequest.policy = true
        accountRepository
            .searchAccount(formRequest)
            .flatMap { accounts ->
                if (argJustAdd) {
                    Single.just(accounts)
                } else {
                    authInteractor.setAuthProgress(AuthState.Progress.COMPLETE).map { accounts }
                }
            }
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { acceptProgressState.value = true }
            .doFinally { acceptProgressState.value = false }
            .lifeSubscribe({
                if (argJustAdd) {
                    router.exit()
                }
            }, {
                handleAddAccountError(it)
            })
    }

    fun onDenyPolicy() {
        showPolicyAction.value = null
    }

    fun onLinkClick(linkId: String) {
        val policy = currentPolicyRequire?.policies?.firstOrNull { it.id == linkId } ?: return
        router.navigateTo(Screens.Document.Detail(policy.url))
    }

    fun onFillProfileClick() {
        profileRepository
            .getProfile()
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { screenProgressState.value = true }
            .doFinally { screenProgressState.value = false }
            .lifeSubscribe({
                form.getField(FIELD_LAST_NAME)?.value = it.lastName
                form.getField(FIELD_FIRST_NAME)?.value = it.firstName
                form.getField(FIELD_PATRONYMIC_NAME)?.value = it.middleName
                form.validateAll(validationsStore.paramsLiveCheck)
                formData.value = form.getFields()
            }, {
                errorHandler.proceed(it)
            })
    }

    fun onDateFieldChanged(field: FormField, date: Date) {
        field.also {
            it.value = getDateValue(date)
        }
        formData.value = form.getFields()
        form.validateAll(validationsStore.paramsLiveCheck)
    }


    fun onDateFieldClick(field: InputFormField) {
        val current =
            try {
                (formatter.parse(field.value))
            } catch (e: Exception) {
                Date()
            }
        pickDateAction.value = DatePick(
            field,
            current
        )
    }

    fun onBackPressed() {
        if (argJustAdd) {
            router.exit()
        } else {
            authInteractor
                .logout()
                .observeOn(schedulersProvider.ui())
                .doOnSubscribe { screenProgressState.value = true }
                .doFinally { screenProgressState.value = false }
                .lifeSubscribe({}, {
                    errorHandler.proceed(it)
                })
        }
    }

    private fun handleAddAccountError(error: Throwable) = when (error) {
        is AccountAddException -> {
            currentPolicyRequire = error.policyRequire
            showPolicyAction.value = PolicyDialogState(
                error.policyRequire.title,
                error.policyRequire.message
            )
        }
        else -> errorHandler.proceed(error)
    }

    fun getDateValue(date: Date?): String = when {
        date != null -> SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(date)
        else -> resourceManager.getString(R.string.form_select_value_none)
    }

    private fun generateFormRequest(): AccountSearchForm? {
        return try {
            AccountSearchForm(
                form.getField(FIELD_ACCOUNT_ID)?.value.orEmpty(),
                form.getField(FIELD_ACCOUNT_NAME)?.value.orEmpty(),
                form.getField(FIELD_FIRST_NAME)?.value.orEmpty(),
                form.getField(FIELD_LAST_NAME)?.value.orEmpty(),
                form.getField(FIELD_PATRONYMIC_NAME)?.value.orEmpty(),
                form.getField(FIELD_PASSPORT_ID)?.value.orEmpty(),
                form.getField(FIELD_PASSPORT_DATE)?.value?.getDateOrNull(formatter)
                    ?: throw java.lang.Exception("Incorrect Date")
            )
        } catch (ex: Exception) {
            errorHandler.proceed(ex)
            null
        }
    }

}
import UIKit

final class MainAssembly {
    class func createModule(parent: Router? = nil) -> MainViewController {
        let module: MainViewController
        module = MainViewController.loadActual()
        let router = MainRouter(view: module, parent: parent)
        module.handler = MainAppCoordinator.shared.container.resolve()
        module.handler.bind(view: module, router: router)
        return module
    }
}

// MARK: - Route

protocol MainRoute {}

extension MainRoute where Self: RouterProtocol {}

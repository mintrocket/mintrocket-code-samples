package com.mintrocket.presentation.presenter.account.create.entity

data class PolicyDialogState(
    val title: String,
    val message: String
)
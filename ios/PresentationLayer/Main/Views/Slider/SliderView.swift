import UIKit

public final class SliderView: LoadableView {
    @IBOutlet private var leftView: SlideView!
    @IBOutlet private var centerView: SlideView!
    @IBOutlet private var rightView: SlideView!
    @IBOutlet private var panRecognizer: UIPanGestureRecognizer!
    @IBOutlet private var widthConstraint: NSLayoutConstraint!

    private var currentIndex: Int = 0 {
        didSet {
            self.indexHandler?(self.currentIndex)
        }
    }

    private var indexHandler: Action<Int>?
    private var payHandler: ActionFunc?
    private var addHandler: ActionFunc?
    private var cardHandler: ActionFunc?
    private var items: [Account] = []
    private let maxOffset: CGFloat = 50
    private let distance: CGFloat = UIScreen.main.bounds.width - 43

    public override func setupNib() {
        super.setupNib()
        self.widthConstraint.constant = UIScreen.main.bounds.width - 50
    }

    func configure(items: [Account], current: Int) {
        self.items = items
        var selected = current

        self.centerView.isLoading = items.isEmpty
        self.centerView.delegate = self

        if selected <= 0 {
            selected = 0
        } else if selected >= self.items.count {
            selected = self.items.count - 1
        }

        self.panRecognizer.isEnabled = true
        if items.count <= 1 {
            self.panRecognizer.isEnabled = false
            self.leftView.isHidden = true
            self.rightView.isHidden = true
        } else if selected == 0 {
            self.leftView.isHidden = true
            self.rightView.isHidden = false
        } else if selected == self.items.count - 1 {
            self.leftView.isHidden = false
            self.rightView.isHidden = true
        } else {
            self.leftView.isHidden = false
            self.rightView.isHidden = false
        }

        self.currentIndex = selected
        self.identityState()
        self.updateSlides()

        self.fadeTransition()
    }

    func indexDidChange(_ action: Action<Int>?) {
        self.indexHandler = action
    }

    func pay(_ action: ActionFunc?) {
        self.payHandler = action
    }

    func add(_ action: ActionFunc?) {
        self.addHandler = action
    }

    func tapped(_ action: ActionFunc?) {
        self.cardHandler = action
    }

    @IBAction func gestureAction(_ sender: UIPanGestureRecognizer) {
        let offset = sender.translation(in: self).x
        let velocity = sender.velocity(in: self).x

        switch sender.state {
        case .ended:
            if velocity > 300 && self.currentIndex > 0 {
                self.rightView.move(offset - distance * 3)
                self.moveRight()
            } else if velocity < -300 && self.currentIndex < self.items.count - 1 {
                self.leftView.move(offset + distance * 3)
                self.moveLeft()
            } else {
                self.end()
            }
            self.centerView.isUserInteractionEnabled = true
            return
        case .cancelled:
            self.centerView.isUserInteractionEnabled = true
            if offset > self.maxOffset {
                self.rightView.move(offset - distance * 3)
                self.moveRight()
            } else if offset < -self.maxOffset {
                self.leftView.move(offset + distance * 3)
                self.moveLeft()
            }
            return
        default:
            self.centerView.isUserInteractionEnabled = false
            self.progress(offset)
            self.moveCenterBehaviour(offset)
            self.moveLeftAndRightBehavior(offset)
        }
    }

    private func progress(_ offset: CGFloat) {
        if (self.currentIndex == 0 && offset > 0 ) ||
        (self.currentIndex == self.items.count - 1 && offset < 0 ) {
            return
        }
        let friction = abs(offset/self.distance)
        self.centerView.state(progress: friction)
        if offset > 0 {
            self.leftView.state(progress: friction)
        } else {
            self.rightView.state(progress: friction)
        }
    }

    private func moveCenterBehaviour(_ offset: CGFloat) {
        if (self.currentIndex != 0 || offset < 0 ) &&
            (self.currentIndex != self.items.count - 1 || offset > 0 ) {
            self.centerView.move(offset)
        } else {
            let modify: CGFloat = offset > 0 ? 1 : -1
            self.centerView.move(modify * sqrt(abs(2*offset)))
        }
    }

    private func moveLeftAndRightBehavior(_ offset: CGFloat) {
        if offset < -self.maxOffset {
            if self.currentIndex < self.items.count - 1 {
                self.panRecognizer.cancel()
            } else {
                self.leftView.move(-sqrt(-2*offset))
            }
            return
        } else {
            if self.currentIndex < self.items.count - 1 || offset > 0 {
                self.leftView.move(offset)
            } else {
                self.leftView.move(-sqrt(-2*offset))
            }
        }

        if offset > self.maxOffset {
            if self.currentIndex > 0 {
                self.panRecognizer.cancel()
            } else {
                self.rightView.move(sqrt(2*offset))
            }
        } else {
            if self.currentIndex > 0 || offset < 0 {
                self.rightView.move(offset)
            } else {
                self.rightView.move(sqrt(2*offset))
            }
        }
    }

    private func end() {
        self.springAnimation(duration: 0.4, animation: {
            self.identityState()
        })
    }

    private func moveRight() {
        self.currentIndex -= 1
        if self.currentIndex == 0 {
            self.rightView.isHidden = true
            self.leftView.isHidden = false
        } else {
            self.rightView.isHidden = false
            self.leftView.isHidden = false
        }

        self.springAnimation(animation: {
            self.centerView.move(self.distance)
            self.centerView.isActive = false
            self.leftView.move(self.distance)
            self.leftView.isActive = true
            self.rightView.move(-2 * self.distance)
            self.rightView.isActive = false
        }, completion: {
            self.updateSlides()
            self.identityState()
            if self.currentIndex == 0 {
                self.rightView.isHidden = false
                self.leftView.isHidden = true
            }
        })
    }

    private func moveLeft() {
        self.currentIndex += 1
        if self.currentIndex == self.items.count - 1 {
            self.rightView.isHidden = false
            self.leftView.isHidden = true
        } else {
            self.rightView.isHidden = false
            self.leftView.isHidden = false
        }

        self.springAnimation(animation: {
            self.centerView.move(-self.distance)
            self.centerView.isActive = false
            self.rightView.move(-self.distance)
            self.rightView.isActive = true
            self.leftView.move(2 * self.distance)
            self.leftView.isActive = false
        }, completion: {
            self.updateSlides()
            self.identityState()
            if self.currentIndex == self.items.count - 1 {
                self.rightView.isHidden = true
                self.leftView.isHidden = false
            }
        })
    }

    private func springAnimation(duration: Double = 0.4,
                                 animation: @escaping ActionFunc,
                                 completion: ActionFunc? = nil) {
        UIView.animate(withDuration: duration,
                       delay: 0,
                       usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 1,
                       options: [.curveEaseInOut],
                       animations: animation,
                       completion: { _ in completion?() })
    }

    private func identityState() {
        self.centerView.isActive = true
        self.leftView.isActive = false
        self.rightView.isActive = false
        self.leftView.transform = .identity
        self.centerView.transform = .identity
        self.rightView.transform = .identity
    }

    private func updateSlides() {
        if self.items.isEmpty {
            return
        }

        let leftIndex = self.currentIndex - 1
        let rightIndex = self.currentIndex + 1

        self.centerView.configure(self.items[self.currentIndex], enabled: true)
        if leftIndex > -1 {
            self.leftView.configure(self.items[leftIndex], enabled: false)
        }
        if rightIndex < self.items.count {
            self.rightView.configure(self.items[rightIndex], enabled: false)
        }
    }
}

extension SliderView: SideViewDelegate {
    public func add() {
        self.addHandler?()
    }

    public func pay() {
        self.payHandler?()
    }

    public func card() {
        self.cardHandler?()
    }
}

extension SliderView: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                                  shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if let current = gestureRecognizer as? UIPanGestureRecognizer {
            let point = current.translation(in: self)

            if abs(point.x) > abs(point.y) {
                return false
            }
        }
        return true
    }

    public override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let current = gestureRecognizer as? UIPanGestureRecognizer {
            let point = current.translation(in: self)

            if abs(point.x) > abs(point.y) {
                return true
            }
            return false
        }
        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
}

private extension UIView {
    func move(_ value: CGFloat) {
        self.transform = CGAffineTransform(translationX: value, y: 0)
    }
}

private extension UIGestureRecognizer {
    func cancel() {
        self.isEnabled.toggle()
        self.isEnabled.toggle()
    }
}

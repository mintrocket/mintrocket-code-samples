package com.mintrocket.zhelezno.ui.addressSelection.premise

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.mintrocket.zhelezno.R
import com.mintrocket.zhelezno.di.DI
import com.mintrocket.zhelezno.di.getDependency
import com.mintrocket.zhelezno.entity.app.address.select.SelectBuilding
import com.mintrocket.zhelezno.entity.app.address.select.SelectHousingComplex
import com.mintrocket.zhelezno.extension.addTextChangeListener
import com.mintrocket.zhelezno.presentation.addressSelection.premise.SelectBuildingsPresenter
import com.mintrocket.zhelezno.presentation.addressSelection.premise.SelectBuildingsView
import com.mintrocket.zhelezno.ui.global.BaseFragment
import com.mintrocket.zhelezno.ui.global.list.SelectBuildingListItem
import com.mintrocket.zhelezno.ui.global.list.SelectHousingComplexListItem
import com.mintrocket.zhelezno.ui.global.list.ListItem
import com.mintrocket.zhelezno.ui.global.list.chat.create.SelectBuildingDelegate
import com.mintrocket.zhelezno.ui.global.list.chat.create.SelectHousingComplexDelegate
import kotlinx.android.synthetic.main.fragment_select_md_buildings.*

class SelectBuildingsFragment : BaseFragment(), SelectBuildingsView {

    companion object {
        const val ARG_RESULT_SCREEN = "result screen"
    }

    override val layoutRes: Int = R.layout.fragment_select_md_buildings

    private val adapter = ItemsAdapter()

    @InjectPresenter
    lateinit var presenter: SelectBuildingsPresenter

    @ProvidePresenter
    fun providePresenter(): SelectBuildingsPresenter =
            getDependency(SelectBuildingsPresenter::class.java, *screenScopes)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            presenter.resultScreen = getString(ARG_RESULT_SCREEN, presenter.resultScreen)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.navigationIcon = ContextCompat.getDrawable(toolbar.context, R.drawable.ic_app_bar_back)
        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = this@SelectBuildingsFragment.adapter
        }

        swipeRefreshLayout.setOnRefreshListener { presenter.loadBuildings() }

        searchField.addTextChangeListener {
            presenter.onSearchQueryChanged(it)
        }
    }

    override fun showHouses(items: List<SelectHousingComplex>) {
        adapter.showItems(items)
    }

    override fun setRefreshing(isRefreshing: Boolean) {
        swipeRefreshLayout.isRefreshing = isRefreshing
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    inner class ItemsAdapter : ListDelegationAdapter<MutableList<ListItem>>() {

        private val clickListener = { item: SelectBuilding ->
            presenter.onItemClick(item)
        }

        init {
            items = mutableListOf()
            delegatesManager.apply {
                addDelegate(SelectHousingComplexDelegate())
                addDelegate(SelectBuildingDelegate(clickListener))
            }
        }

        fun showItems(newItems: List<SelectHousingComplex>) {
            items.clear()
            newItems.forEach { complex ->
                items.add(SelectHousingComplexListItem(complex))
                items.addAll(complex.buildings.map { SelectBuildingListItem(it) })
            }
            notifyDataSetChanged()
        }
    }
}

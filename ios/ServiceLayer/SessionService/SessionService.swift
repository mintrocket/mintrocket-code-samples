import DITranquillity
import Foundation
import RxCocoa
import RxSwift

class SessionServicePart: DIPart {
    static func load(container: DIContainer) {
        container.register(SessionServiceImp.init)
            .as(SessionService.self)
            .as(SessionRefreshService.self)
            .lifetime(.single)
    }
}

public enum SessionStatus: String {
    case unauthorized
    case authorized
    case registration
    case account
}

typealias ProfileStatus = (valid: Bool, password: Bool)

protocol SessionRefreshService {
    func refreshSession() -> Single<Void>
    func forceLogout()
}

protocol SessionService {
    func sessionStatus() -> Observable<SessionStatus>
    func currentStatus() -> SessionStatus

    func check(phone: String) -> Single<PhoneCheckResult>
    func verify(code: String, uuid: String) -> Single<ProfileStatus>
    func fetchTimeBeforResend() -> Observable<Int>

    func forget(phone: String) -> Single<PhoneCheckResult>
    func forgetVerify(code: String, uuid: String) -> Single<Void>
    func repair(password: String, confirm: String) -> Single<SessionStatus>

    func login(phone: String, password: String) -> Single<Bool>
    func register(user: User, password: String, confirm: String) -> Single<Void>
    func addFirst(accout: AddAccountData, policy: Bool, name: String) -> Single<Void>
    func checkProfile() -> Single<ProfileStatus>
    func checkAccounts() -> Single<Bool>
    func logout()
}

final class SessionServiceImp: SessionService {
    let schedulers: SchedulerProvider
    let backendRepository: BackendRepository
    let authRepository: AuthRepository
    let profileService: ProfileSerive
    let accountService: AccountService
    let clearManager: ClearableManager

    let resendDelay: TimeInterval = 120
    var startTimerDate: TimeInterval = 0
    var timerBag: DisposeBag!
    let bag: DisposeBag = DisposeBag()

    let timeBeforeNextSms: BehaviorRelay<Int> = BehaviorRelay(value: 0)

    init(schedulers: SchedulerProvider,
         backendRepository: BackendRepository,
         authRepository: AuthRepository,
         clearManager: ClearableManager,
         profileService: ProfileSerive,
         accountService: AccountService) {
        self.schedulers = schedulers
        self.backendRepository = backendRepository
        self.authRepository = authRepository
        self.clearManager = clearManager
        self.profileService = profileService
        self.accountService = accountService

        self.accountService
            .accountsSequence()
            .subscribe(onNext: { [weak self] accounts in
                if accounts.isEmpty {
                    self?.authRepository.set(status: .account)
                }
            })
            .disposed(by: self.bag)
    }

    func currentStatus() -> SessionStatus {
        return self.authRepository.getStatus()
    }

    func sessionStatus() -> Observable<SessionStatus> {
        return self.authRepository
            .sessionStatusSequence()
            .observeOn(self.schedulers.main)
    }

    func check(phone: String) -> Single<PhoneCheckResult> {
        return Single.deferred { [unowned self] in
            self.stopTimer()
            self.startTimer()
            self.authRepository.set(phone: phone.digits)
            return self.backendRepository
                .request(CheckRequest(phone: phone.digits))
        }
        .subscribeOn(self.schedulers.background)
        .observeOn(self.schedulers.main)
    }

    func forget(phone: String) -> Single<PhoneCheckResult> {
        return Single.deferred { [unowned self] in
            self.stopTimer()
            self.startTimer()
            return self.backendRepository
                .request(ForgetRequest(phone: phone.digits))
        }
            .subscribeOn(self.schedulers.background)
            .observeOn(self.schedulers.main)
    }

    func repair(password: String, confirm: String) -> Single<SessionStatus> {
        return self.profileService.set(password: password,
                                       confirm: confirm)
            .flatMap { [unowned self] in
                return self.accountService
                    .fetchAccounts()
                    .map { [weak self] items in
                        self?.authRepository.save()
                        if items.isEmpty == false {
                            self?.authRepository.set(status: .authorized)
                            return .authorized
                        }
                        self?.authRepository.set(status: .account)
                        return .account
                    }
            }
    }

    func forgetVerify(code: String, uuid: String) -> Single<Void> {
        return Single.deferred { [unowned self] in
            return self.backendRepository
                .request(VerifyRequest(code: code, uuid: uuid))
                .do(onSuccess: { [weak self] data in
                    self?.authRepository.set(refreshToken: data.refreshToken)
                    self?.authRepository.set(accessToken: data.accessToken)
                })
                .asVoid()
        }
            .subscribeOn(self.schedulers.background)
            .observeOn(self.schedulers.main)
    }

    func verify(code: String, uuid: String) -> Single<ProfileStatus> {
        return Single.deferred { [unowned self] in
            var profileStatus: ProfileStatus?
            return self.backendRepository
                .request(VerifyRequest(code: code, uuid: uuid))
                .flatMap {  [unowned self] data -> Single<ProfileStatus> in
                    self.authRepository.set(refreshToken: data.refreshToken)
                    self.authRepository.set(accessToken: data.accessToken)
                    return self.checkProfile()
                }
                .flatMap { [unowned self] data -> Single<Bool> in
                    profileStatus = data
                    if data.valid && data.password {
                        return self.checkAccounts()
                    }
                    return .just(false)
                }
                .map { _ in profileStatus! }
        }
        .subscribeOn(self.schedulers.background)
        .observeOn(self.schedulers.main)
    }

    func fetchTimeBeforResend() -> Observable<Int> {
        return self.timeBeforeNextSms
            .asObservable()
            .observeOn(self.schedulers.main)
    }

    func login(phone: String, password: String) -> Single<Bool> {
        return Single.deferred { [unowned self] in
            return self.backendRepository
                .request(LoginRequest(phone: phone.digits, password: password))
                .flatMap { [unowned self] data -> Single<Bool> in
                    self.authRepository.set(refreshToken: data.refreshToken)
                    self.authRepository.set(accessToken: data.accessToken)
                    self.authRepository.set(phone: phone.digits)
                    return self.checkAccounts()
                }
        }
            .subscribeOn(self.schedulers.background)
            .observeOn(self.schedulers.main)
    }

    func register(user: User, password: String, confirm: String) -> Single<Void> {
        return self.profileService
            .update(user: user)
            .flatMap { [unowned self] in
                if $0.hasPassword {
                    return .just(())
                } else {
                    return self.profileService.set(password: password,
                                                   confirm: confirm)
                }
            }
            .do(onSuccess: { [weak self] in
                self?.authRepository.set(status: .account)
            })
    }

    func addFirst(accout: AddAccountData, policy: Bool, name: String) -> Single<Void> {
        return self.accountService
            .add(accout: accout, policy: policy, name: name)
            .do(onSuccess: { [weak self] _ in
                self?.authRepository.set(status: .authorized)
            })
            .asVoid()
    }

    func checkAccounts() -> Single<Bool> {
        return self.accountService
            .fetchAccounts()
            .map { [weak self] items in
                self?.authRepository.save()
                if items.isEmpty == false {
                    self?.authRepository.set(status: .authorized)
                    return true
                }
                self?.authRepository.set(status: .account)
                return false
            }
    }

    func checkProfile() -> Single<ProfileStatus> {
        return self.profileService
            .fetchUser()
            .map { ProfileStatus($0.isValidated, $0.hasPassword) }
            .flatMap { [unowned self] data in
                if !data.valid {
                    self.authRepository.set(status: .registration)
                    self.authRepository.save()
                } else if data.password {
                    self.authRepository.set(status: .account)
                    self.authRepository.save()
                }
                return .just(data)
            }
    }

    func logout() {
        self.backendRepository
            .request(LogoutRequest())
            .subscribe()
            .disposed(by: self.bag)
        self.forceLogout()
    }

    // MARK: - Timer

    private func startTimer() {
        self.startTimerDate = Date().timeIntervalSince1970
        self.timerBag = DisposeBag()
        self.timeBeforeNextSms.accept(Int(self.resendDelay))
        Observable<Int>.interval(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [unowned self] _ in
                let elapsed = Date().timeIntervalSince1970 - self.startTimerDate
                let left = Int(max(self.resendDelay - elapsed, 0))

                self.timeBeforeNextSms.accept(left)
                if left == 0 {
                    self.timerBag = nil
                }
            })
            .disposed(by: self.timerBag)
    }

    private func stopTimer() {
        self.timerBag = nil
        if self.timeBeforeNextSms.value != 0 {
            self.timeBeforeNextSms.accept(0)
        }
    }
}

extension SessionServiceImp: SessionRefreshService {
    func refreshSession() -> Single<Void> {
        return Single.deferred { [unowned self] in
            guard let phone = self.authRepository.getPhone() else {
                return .error(AppError.unexpectedError(message: "Phone missing"))
            }
            let rToken = self.authRepository.getRefreshToken()
            return self.backendRepository
                .request(RefreshRequest(phone: phone,
                                        refreshToken: rToken))
                .do(onSuccess: { [weak self] data in
                    self?.authRepository.set(refreshToken: data.refreshToken)
                    self?.authRepository.set(accessToken: data.accessToken)
                    self?.authRepository.set(phone: phone)
                    self?.authRepository.save()
                })
                .asVoid()
        }
            .subscribeOn(self.schedulers.background)
            .observeOn(self.schedulers.main)
    }

    func forceLogout() {
        self.clearManager.clear()
        self.authRepository.set(status: .unauthorized)
    }
}

package com.mintrocket.presentation.screen.account.list

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.LinearLayoutManager
import app.modules.recycler_screen.DifferAdapter
import com.mintrocket.presentation.R
import com.mintrocket.presentation.common.BackController
import com.mintrocket.presentation.common.DimensionHelper
import com.mintrocket.presentation.common.form.field.InputFormField
import com.mintrocket.presentation.extension.*
import com.mintrocket.presentation.presenter.account.list.AccountListViewModel
import com.mintrocket.presentation.presenter.account.list.entity.AccountState
import com.mintrocket.presentation.screen.BaseAppFragment
import com.mintrocket.presentation.screen.account.list.adapter.AccountDelegate
import com.mintrocket.presentation.screen.account.list.entity.AccountListItem
import com.mintrocket.presentation.widget.form.FormInputTextView
import kotlinx.android.synthetic.main.dialog_account_name.view.*
import kotlinx.android.synthetic.main.fragment_account_list.*
import kotlinx.android.synthetic.main.fragment_app_base.*
import javax.inject.Inject

class AccountListFragment : BaseAppFragment() {

    private val accountsAdapter by lazy {
        DifferAdapter().apply {
            addDelegate(AccountDelegate({
                viewModel.onAccountClick(it.item)
            }, {
                viewModel.onAccountEditClick(it.item)
            }, {
                viewModel.onAccountDeleteClick(it.item)
            }))
        }
    }

    private val viewModel by viewModel(AccountListViewModel::class.java)

    @Inject
    lateinit var backController: BackController

    override val layoutResource: Int
        get() = R.layout.fragment_account_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        baseToolbar.setNavigationOnClickListener { backController.back() }
        baseToolbar.setNavigationIcon(R.drawable.ic_back)
        baseToolbar.title = getString(R.string.screen_account_list_title)
        baseFragmentContainer.setRoundColor(Color.TRANSPARENT)

        baseToolbar.menu
            .add(R.string.screen_account_list_add)
            .setIcon(context!!.getCompatDrawable(R.drawable.ic_home_account_add))
            .setOnMenuItemClickListener {
                viewModel.onAddClick()
                return@setOnMenuItemClickListener true
            }
            .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)

        accountRecycler.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = accountsAdapter
        }

        accountSwipeRefresh.setOnRefreshListener { viewModel.refresh() }

        viewModel.progressAction.subscribe {
            accountSwipeRefresh.isRefreshing = it
        }

        viewModel.screenProgressAction.subscribe {
            showProgress(it)
        }

        viewModel.accountsData.subscribe {
            accountsAdapter.items = it.map { AccountListItem(it) }
        }

        viewModel.deleteShowAction.subscribe {
            showDeleteDialog(it)
        }
    }

    override fun updateDimens(dimensions: DimensionHelper.Dimensions) {
        super.updateDimens(dimensions)
        accountRecycler.updatePadding(bottom = dimensions.bottom)
    }

    private fun showDeleteDialog(item: AccountState) {
        AlertDialog.Builder(context!!)
            .setTitle("Удалить лицевой счёт")
            .setMessage("Вы действительно хотите удалить лицевой счёт?")
            .setPositiveButton("Удалить") { dialog, which ->
                viewModel.onAcceptDelete(item)
            }
            .setNegativeButton("Отмена", null)
            .show()
            .apply {
                getButton(AlertDialog.BUTTON_POSITIVE)
                    .setTextColor(context.getCompatColor(R.color.light_paletteRed))
            }
    }

}
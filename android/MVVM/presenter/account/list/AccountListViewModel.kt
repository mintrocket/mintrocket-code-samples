package com.mintrocket.presentation.presenter.account.list

import androidx.lifecycle.MutableLiveData
import com.mintrocket.domain.common.SchedulersProvider
import com.mintrocket.domain.entity.account.Account
import com.mintrocket.domain.entity.account.AccountsMetaInfo
import com.mintrocket.domain.entity.auth.AuthState
import com.mintrocket.domain.interactor.AuthInteractor
import com.mintrocket.domain.repository.AccountRepository
import com.mintrocket.presentation.common.ActionLiveData
import com.mintrocket.presentation.common.ErrorHandler
import com.mintrocket.presentation.navigation.Screens
import com.mintrocket.presentation.presenter.BaseViewModel
import com.mintrocket.presentation.presenter.account.list.entity.AccountState
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class AccountListViewModel @Inject constructor(
    private val accountRepository: AccountRepository,
    private val authInteractor: AuthInteractor,
    private val schedulersProvider: SchedulersProvider,
    private val errorHandler: ErrorHandler,
    private val router: Router
) : BaseViewModel() {

    val accountsData = MutableLiveData<List<AccountState>>()
    val progressAction = ActionLiveData<Boolean>()
    val screenProgressAction = ActionLiveData<Boolean>()
    val deleteShowAction = ActionLiveData<AccountState>()

    override fun onCreate() {
        super.onCreate()
        accountRepository
            .observeAccounts()
            .map { it.filter { it.policy } }
            .observeOn(schedulersProvider.ui())
            .lifeSubscribe({
                progressAction.value = false
                setAccountData(it)
            }, {
                errorHandler.proceed(it)
            })

        refresh()
    }

    fun refresh() {
        accountRepository
            .getAccounts()
            .map { it.filter { it.policy } }
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { progressAction.value = true }
            .doFinally { progressAction.value = false }
            .lifeSubscribe({}, {
                errorHandler.proceed(it)
            })
    }

    fun onAcceptDelete(item: AccountState) {
        accountRepository
            .deleteAccount(item.id)
            .andThen(authInteractor.setAuthProgress(AuthState.Progress.ACCOUNT))
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { screenProgressAction.value = true }
            .doFinally { screenProgressAction.value = false }
            .lifeSubscribe({}, {
                errorHandler.proceed(it)
            })
    }

    fun onAddClick() {
        router.navigateTo(Screens.Account.Create(true))
    }

    fun onAccountClick(item: AccountState) {
        accountRepository.setSelectAccountInfo(AccountsMetaInfo(item.id))
            .subscribeOn(schedulersProvider.io())
            .lifeSubscribe({
                router.navigateTo(Screens.Account.Info(item.id))
            }, {
                errorHandler.proceed(it)
            })
        //router.navigateTo(Screens.PlaceholderScreen("Информация о лицевом счете"))
    }

    fun onAccountEditClick(item: AccountState) {
        router.navigateTo(Screens.Account.Name(item.id))
    }

    fun onAccountDeleteClick(item: AccountState) {
        deleteShowAction.value = item
    }

    private fun setAccountData(accounts: List<Account>) {
        accountsData.value = accounts.map {
            AccountState(
                it.id,
                (it.accountName ?: it.address) ?: "Моя квартира",
                "№${it.accountNumber.orEmpty()}",
                it.address.orEmpty()
            )
        }
    }
}
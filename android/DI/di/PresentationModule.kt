package ru.ingrad.home.di

import android.content.Context
import ru.ingrad.home.common.OkHttpImageDownloader
import com.mintrocket.presentation.common.intent.IntentHelper
import com.mintrocket.presentation.common.intent.IntentHelperImpl
import com.mintrocket.presentation.navigation.CiceroneHolder
import toothpick.config.Module

class PresentationModule(context: Context) : Module() {

    init {
        bind(CiceroneHolder::class.java).toInstance(CiceroneHolder())

        bind(IntentHelper::class.java).to(IntentHelperImpl::class.java).singletonInScope()

        bind(OkHttpImageDownloader::class.java).singletonInScope()
    }
}
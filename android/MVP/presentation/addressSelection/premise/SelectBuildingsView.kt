package com.mintrocket.zhelezno.presentation.addressSelection.premise

import com.mintrocket.zhelezno.presentation.global.BaseMvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.mintrocket.zhelezno.entity.app.address.select.SelectHousingComplex

@StateStrategyType(AddToEndSingleStrategy::class)
interface SelectBuildingsView : BaseMvpView {
    fun showHouses(items: List<SelectHousingComplex>)
    fun setRefreshing(isRefreshing: Boolean)
}

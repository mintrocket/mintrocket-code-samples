package com.mintrocket.zhelezno.ui.addressSelection

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import com.mintrocket.zhelezno.R
import com.mintrocket.zhelezno.Screens
import com.mintrocket.zhelezno.di.DI
import com.mintrocket.zhelezno.di.injectDependencies
import com.mintrocket.zhelezno.model.system.AppNavigator
import com.mintrocket.zhelezno.presentation.addressSelection.AddressSelectionMode
import com.mintrocket.zhelezno.ui.authorization.AuthActivity
import com.mintrocket.zhelezno.ui.global.BaseActivity
import com.mintrocket.zhelezno.ui.launch.MainActivity
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.commands.Replace
import toothpick.Toothpick
import javax.inject.Inject

class AddressSelectionActivity : BaseActivity() {

    companion object {
        private const val ARG_MODE = "mode"

        fun getStartIntent(context: Context, mode: AddressSelectionMode) = Intent(context, AddressSelectionActivity::class.java).apply {
            putExtra(ARG_MODE, mode)
        }
    }

    override val localScopes: Array<String> = arrayOf(DI.ADDRESS_SELECTION_SCOPE)

    override val layoutRes = R.layout.activity_container

    @Inject
    lateinit var navigationHolder: NavigatorHolder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val mode = intent.getSerializableExtra(ARG_MODE) as? AddressSelectionMode
                ?: AddressSelectionMode.INITIAL
        navigator.applyCommands(arrayOf(Replace(Screens.ADDRESS_SELECTION, mode)))
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigationHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigationHolder.removeNavigator()
        super.onPause()
    }

    private val navigator = object : AppNavigator(this, R.id.container) {
        override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? = when (screenKey) {
            Screens.MAIN -> Intent(this@AddressSelectionActivity, MainActivity::class.java)
            Screens.AUTH -> Intent(this@AddressSelectionActivity, AuthActivity::class.java)
            else -> null
        }

        override fun createFragment(screenKey: String?, data: Any?): Fragment? = when (screenKey) {
            Screens.ADDRESS_SELECTION -> AddressSelectionFragment.newInstance(data as? AddressSelectionMode
                    ?: AddressSelectionMode.INITIAL)
            else -> null
        }
    }
}

package com.mintrocket.presentation.di

import com.mintrocket.presentation.navigation.CiceroneHolder
import com.mintrocket.presentation.navigation.ScopedRouter
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class RouterModule(screenScope: String, parentScreenScope: String?) : Module() {

    init {
        val parentRouter = parentScreenScope?.let { DI.get(ScopedRouter::class.java, it) }
        val cicerone = DI.get(CiceroneHolder::class.java).getCicerone(
            screenScope,
            parentRouter
        )
        bind(ScopedRouter::class.java).toInstance(cicerone.router)
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)
    }
}
import UIKit.UIDevice

public struct RefreshRequest: BackendAPIRequest {
    typealias ResponseObject = SessionData

    private(set) var endpoint: String = "auth/refresh"
    private(set) var method: NetworkManager.Method = .POST
    private(set) var parameters: [String: Any]
    private(set) var headers: [String: String] = [:]

    init(phone: String, refreshToken: String) {
        let deviceId = UIDevice.deviceId

        self.parameters = [
            "device_id": deviceId,
            "phone": phone,
            "refresh_token": refreshToken
        ]
        if let token = TokenFactory.create(phone: phone, deviceId: deviceId) {
            self.headers = [
                "Authorization": token
            ]
        }
    }
}

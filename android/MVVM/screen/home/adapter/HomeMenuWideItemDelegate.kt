package com.mintrocket.presentation.screen.home.adapter

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import androidx.core.widget.ImageViewCompat
import app.modules.recycler_screen.BaseAdapterDelegate
import com.mintrocket.presentation.R
import com.mintrocket.presentation.extension.getColorFromAttr
import com.mintrocket.presentation.extension.getCompatColor
import com.mintrocket.presentation.extension.isLightTheme
import com.mintrocket.presentation.extension.setCompatDrawable
import com.mintrocket.presentation.screen.home.entity.HomeMenuListItem
import com.mintrocket.presentation.screen.home.entity.HomeMenuWideListItem
import kotlinx.android.synthetic.main.item_home_menu_wide.*
import kotlin.reflect.KClass

open class HomeMenuWideItemDelegate(
    private val clickListener: (HomeMenuWideListItem) -> Unit
) : BaseAdapterDelegate<HomeMenuWideListItem>(
    R.layout.item_home_menu_wide,
    HomeMenuWideListItem::class
) {

    @SuppressLint("ResourceType")
    override fun bindData(item: HomeMenuWideListItem, holder: BaseViewHolder<HomeMenuWideListItem>) {
        super.bindData(item, holder)
        val data = item.item
        holder.apply {

            val isColored = data.colored || !containerView.context.isLightTheme()

            val cardBackgroundAttr = if (isColored) {
                R.attr.colorAccent
            } else {
                R.attr.colorSurface
            }
            val dotColorAttr = if (isColored) {
                R.attr.colorOnPrimary
            } else {
                R.attr.colorAccent
            }

            itemMenuCard.setCardBackgroundColor(itemMenuCard.context.getColorFromAttr(cardBackgroundAttr))
            ImageViewCompat.setImageTintList(
                itemMenuDot,
                ColorStateList.valueOf(itemMenuCard.context.getColorFromAttr(dotColorAttr))
            )

            itemMenuIcon.setCompatDrawable(data.icRes)
            itemMenuTitle.text = data.title
            itemMenuDesc.text = data.desc
            itemMenuDot.isVisible = data.dotVisible
            containerView.setOnClickListener { clickListener.invoke(item) }
        }
    }

    override fun applyPayloads(
        item: HomeMenuWideListItem,
        holder: BaseViewHolder<HomeMenuWideListItem>,
        payloads: List<Any>
    ) {
        super.applyPayloads(item, holder, payloads)
        bindData(item, holder)
    }
}
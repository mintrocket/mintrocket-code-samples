package ru.ingrad.home

import android.app.Application
import android.content.Context
import android.graphics.Bitmap
import android.os.Handler
import android.os.Looper
import androidx.multidex.MultiDex
import app.modules.app_messenger.AppMessagesObserver
import ru.ingrad.home.common.OkHttpImageDownloader
import ru.ingrad.home.di.AppModule
import ru.ingrad.home.di.DataModule
import ru.ingrad.home.di.DomainModule
import ru.ingrad.home.di.PresentationModule
import com.mintrocket.domain.repository.TempFileRepository
import com.mintrocket.presentation.di.DI
import com.mintrocket.presentation.di.MessengerModule
import com.mintrocket.presentation.di.RouterModule
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import timber.log.Timber.DebugTree
import toothpick.Toothpick

class App : Application() {
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }

        val scheduler = AndroidSchedulers.from(Looper.getMainLooper(), false)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler }
        RxAndroidPlugins.setMainThreadSchedulerHandler { scheduler }
        val appScope = Toothpick.openScope(DI.APP_SCOPE)
        appScope.installModules(
            AppModule(this),
            DomainModule(),
            DataModule(this),
            PresentationModule(this),
            RouterModule(DI.APP_SCOPE, null),
            MessengerModule(this)
        )
        initImageLoader(this)

        DI.get(AppMessagesObserver::class.java).apply {
            start()
            resume()
        }

        val disposable = DI.get(TempFileRepository::class.java)
            .clear()
            .subscribe({}, {
                it.printStackTrace()
            })
    }

    private val defaultOptionsUIL: DisplayImageOptions.Builder = DisplayImageOptions.Builder()
        .cacheInMemory(true)
        .resetViewBeforeLoading(true)
        .cacheOnDisk(true)
        .bitmapConfig(Bitmap.Config.ARGB_8888)
        .handler(Handler())
        .displayer(FadeInBitmapDisplayer(500, true, true, false))


    private fun initImageLoader(context: Context) {
        val imageDownloader = DI.get(OkHttpImageDownloader::class.java)
        val maxMemory = (Runtime.getRuntime().maxMemory()).toInt()
        val memoryCacheSize = maxMemory / 8

        val config = ImageLoaderConfiguration.Builder(context)
            .threadPoolSize(5)
            .threadPriority(Thread.MIN_PRIORITY)
            .denyCacheImageMultipleSizesInMemory()
            .memoryCache(UsingFreqLimitedMemoryCache(memoryCacheSize))
            .diskCacheSize(25 * 1024 * 1024)
            .diskCacheFileNameGenerator(HashCodeFileNameGenerator())
            .defaultDisplayImageOptions(defaultOptionsUIL.build())
            .imageDownloader(imageDownloader)
            .build()
        ImageLoader.getInstance().init(config)
    }
}
package com.mintrocket.presentation.di

import android.content.Context
import app.modules.app_messenger.AppMessagesObserver
import app.modules.app_messenger.AppMessenger
import com.mintrocket.presentation.common.ErrorHandler
import toothpick.config.Module

class MessengerModule(val context: Context) : Module() {

    init {
        bind(Context::class.java).toInstance(context)
        bind(AppMessagesObserver::class.java).singletonInScope()
        bind(AppMessenger::class.java).singletonInScope()
        bind(ErrorHandler::class.java).singletonInScope()
    }

}
package com.mintrocket.presentation.screen.home

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.doOnPreDraw
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import androidx.viewpager2.widget.ViewPager2
import app.modules.recycler_screen.DifferAdapter
import com.mintrocket.presentation.R
import com.mintrocket.presentation.common.DimensionHelper
import com.mintrocket.presentation.extension.getDimenPx
import com.mintrocket.presentation.extension.toDp
import com.mintrocket.presentation.extension.viewModel
import com.mintrocket.presentation.presenter.home.HomeViewModel
import com.mintrocket.presentation.presenter.home.entity.HomeMenuItemState
import com.mintrocket.presentation.screen.BaseScreenFragment
import com.mintrocket.presentation.screen.home.adapter.HomeAccountItemDelegate
import com.mintrocket.presentation.screen.home.adapter.HomeAccountLoaderDelegate
import com.mintrocket.presentation.screen.home.adapter.HomeMenuItemDelegate
import com.mintrocket.presentation.screen.home.adapter.HomeMenuWideItemDelegate
import com.mintrocket.presentation.screen.home.entity.HomeAccountListItem
import com.mintrocket.presentation.screen.home.entity.HomeMenuListItem
import com.mintrocket.presentation.screen.home.entity.HomeMenuWideListItem
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : BaseScreenFragment() {

    private val accountAdapter by lazy {
        DifferAdapter().apply {
            addDelegate(
                HomeAccountItemDelegate({
                    viewModel.onAccountClick(it.item)
                }, {
                    viewModel.onAddAccountClick()
                }, {
                    viewModel.onAccountActionClick(it.item)
                })
            )
            addDelegate(HomeAccountLoaderDelegate())
        }
    }

    private val menuAdapter by lazy {
        DifferAdapter().apply {
            addDelegate(HomeMenuItemDelegate {
                viewModel.onMenuItemClick(it.item)
            })
            addDelegate(HomeMenuWideItemDelegate {
                viewModel.onMenuItemClick(it.item)
            })
        }
    }

    private val scrollableContentLayoutListener =
        View.OnLayoutChangeListener { v, _, _, _, _, _, _, _, _ ->
            val overlayHeight = homeMainContent.top + v.getDimenPx(R.dimen.content_radius)
            homeImageContainer.updateLayoutParams<ConstraintLayout.LayoutParams> {
                height = overlayHeight
            }
            homeImageContainer.post {
                homeImageContainer ?: return@post
                /*TransitionManager.beginDelayedTransition(root, AutoTransition().apply {
                    excludeChildren(homeScrollView, true)
                })*/
                homeImageContainer.updateLayoutParams<ConstraintLayout.LayoutParams> {
                    height = overlayHeight
                }
            }
        }

    private val pagerListener = object : ViewPager2.OnPageChangeCallback() {
        private var hasFirstState = false

        override fun onPageScrollStateChanged(state: Int) {
            super.onPageScrollStateChanged(state)
            hasFirstState = true
        }

        override fun onPageSelected(position: Int) {
            if (!hasFirstState) {
                return
            }
            viewModel.onAccountIndexChanged(position)
        }
    }

    private val imageLoaderOptions = DisplayImageOptions.Builder()
        .cacheInMemory(true)
        .cacheOnDisk(true)
        .displayer(SimpleBitmapDisplayer())
        //.resetViewBeforeLoading(true)
        .bitmapConfig(Bitmap.Config.ARGB_8888)
        .showImageOnFail(R.drawable.image)
        .build()

    private val viewModel by viewModel(HomeViewModel::class.java)

    private var homeAccountRecycler: RecyclerView? = null

    override val layoutResource: Int
        get() = R.layout.fragment_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeAccountViewpager.apply {
            orientation = ViewPager2.ORIENTATION_HORIZONTAL
            adapter = accountAdapter

            homeAccountIndicator.setViewPager(this)
            accountAdapter.registerAdapterDataObserver(homeAccountIndicator.adapterDataObserver)

            setPageTransformer(AccountItemsTransformer(context))
            registerOnPageChangeCallback(pagerListener)
        }

        homeAccountRecycler = (0 until homeAccountViewpager.childCount)
            .mapNotNull { homeAccountViewpager.getChildAt(it) as RecyclerView? }
            .first()
        homeAccountRecycler?.apply {
            // Исправляем поведение ViewPager2 (разрешает юзать только height=match_parent чайлды ресайклера)
            clearOnChildAttachStateChangeListeners()
            clipToPadding = false
            isNestedScrollingEnabled = false
            overScrollMode = View.OVER_SCROLL_NEVER
            updatePadding(
                21.toDp(this),
                24.toDp(this),
                21.toDp(this),
                10.toDp(this)
            )
        }

        homeMenuRecycler.apply {
            layoutManager = GridLayoutManager(context, 2).apply {
                spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int = when {
                        position <= 3 -> 1
                        else -> 2
                    }
                }
            }
            adapter = menuAdapter
            //itemAnimator = null
        }


        // Затенение статусбара
        homeScrollView.setOnScrollChangeListener { v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            overlayStatusBar ?: return@setOnScrollChangeListener
            v ?: return@setOnScrollChangeListener

            val percent = (scrollY / overlayStatusBar.height.toFloat()).coerceIn(0f, 1f)
            if (overlayStatusBar.alpha != percent) {
                overlayStatusBar.alpha = percent
            }
        }

        // Первичное значение высоты для фонового изображения
        homeScrollableContent.doOnPreDraw {
            homeImageContainer ?: return@doOnPreDraw
            homeImageContainer.updateLayoutParams<ConstraintLayout.LayoutParams> {
                height = homeMainContent.top + it.getDimenPx(R.dimen.content_radius)
            }
        }
        // Отслеживаем высоту для фонового изображения
        homeScrollableContent.addOnLayoutChangeListener(scrollableContentLayoutListener)

        homeImageContainer.apply {
            homeImageContainer.inAnimation = AlphaAnimation(0f, 1f).apply {
                duration = 250
            }
            homeImageContainer.outAnimation = AlphaAnimation(1f, 0f).apply {
                interpolator = AccelerateInterpolator()
                duration = 500
            }
        }
        homeImage1.setImageResource(R.drawable.image)
        homeImage2.setImageResource(R.drawable.image)

        homeSwipeRefresh.setOnRefreshListener {
            viewModel.refresh()
        }

        homeCounterAlertContainer.setOnClickListener { viewModel.onAlertClick() }

        viewModel.refreshAction.subscribe {
            homeSwipeRefresh.isRefreshing = it
        }

        viewModel.menuStateData.subscribe {
            menuAdapter.items = it.mapIndexed { index: Int, homeMenuItemState: HomeMenuItemState ->
                if (index <= 3) {
                    HomeMenuListItem(homeMenuItemState)
                } else {
                    HomeMenuWideListItem(homeMenuItemState)
                }
            }
        }


        viewModel.accountStateData.subscribe {
            homeAccountViewpager.unregisterOnPageChangeCallback(pagerListener)
            accountAdapter.items = it.map { HomeAccountListItem(it) }
            homeAccountIndicator.isVisible = it.size > 1
            homeAccountViewpager.registerOnPageChangeCallback(pagerListener)
        }
        viewModel.currentAccountIndex.subscribe {
            if (it != homeAccountViewpager.currentItem) {
                homeAccountViewpager.setCurrentItem(it, false)
            }
        }
        var currentImage: String? = null
        viewModel.currentHomeImage.subscribe {
            if (currentImage == it) return@subscribe
            currentImage = it
            val imageView = homeImageContainer.nextView as ImageView
            if (it.isNullOrEmpty()) {
                imageView.setImageResource(R.drawable.image)
                homeImageContainer?.showNext()
            } else {
                ImageLoader.getInstance()
                    .displayImage(
                        it,
                        imageView,
                        imageLoaderOptions,
                        object : SimpleImageLoadingListener() {
                            override fun onLoadingComplete(
                                imageUri: String?,
                                view: View?,
                                loadedImage: Bitmap?
                            ) {
                                super.onLoadingComplete(imageUri, view, loadedImage)
                                homeImageContainer?.showNext()
                            }
                        })
            }
        }

        var alertShow = false
        viewModel.alertData.subscribe {
            val show = !it.isNullOrEmpty()
            if (alertShow == show) return@subscribe
            alertShow = show
            showCountersAlert(show)
            homeAlertText.text = it
        }
    }

    override fun isLightStatusBar(): Boolean {
        return false
    }

    private fun showCountersAlert(show: Boolean) {
        TransitionManager.beginDelayedTransition(homeMainContent, AutoTransition().apply {
            excludeChildren(R.id.homeMenuContainer, true)
        })
        homeMenuContainer.updateLayoutParams<ConstraintLayout.LayoutParams> {
            if (show) {
                topToBottom = R.id.homeAlertSpace
                topToTop = ConstraintLayout.LayoutParams.UNSET
            } else {
                topToBottom = ConstraintLayout.LayoutParams.UNSET
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
            }
        }
        //homeCounterAlert.alpha = if (show) 1f else 0f
        //homeCounterAlert.animate().alpha(if (show) 1f else 0f).setDuration(150).start()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        homeAccountRecycler = null
        homeScrollableContent.removeOnLayoutChangeListener(scrollableContentLayoutListener)
    }

    override fun updateDimens(dimensions: DimensionHelper.Dimensions) {
        super.updateDimens(dimensions)
        homeMenuRecycler.updatePadding(bottom = dimensions.bottom + 10.toDp(homeMenuRecycler))
        overlayStatusBar.isVisible = dimensions.top > 0
        overlayStatusBar.updateLayoutParams {
            height = dimensions.top
        }
        homeToolbarHeader.updateLayoutParams<ViewGroup.MarginLayoutParams> {
            topMargin = dimensions.top
        }
    }
}
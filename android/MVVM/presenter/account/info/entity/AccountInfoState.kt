package com.mintrocket.presentation.presenter.account.info.entity

data class AccountInfoState(
    val id: Int,

    val image: String?,
    val name: String,
    val number: String,
    val userName: String,
    val address: String,

    val sum: String,
    val sumDesc: String,
    val payBtn: String,

    val totalArea: String,
    val liveArea: String,
    val registeredPeople: String,
    val livePeople: String
)